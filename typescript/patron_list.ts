export module patron_list {
    enum sort_dir {
        ASC
        ,DESC
    };

    class patron {
        Name: string;
        ID: number;
        Email: string;
        LastActivity: number;
    }

    let current_page_index: number;
    let patrons_filtered: patron[];
    let current_sort: string;
    let current_sort_dir: sort_dir;

    export let patrons_json: patron[];

    export function on_load(): void
    {
        current_page_index = 0;
        current_sort = "LastActivity";
        current_sort_dir = sort_dir.DESC;

        button_style();
        
        patrons_filtered = patrons_json;
        refresh(current_page_index);
    }

    export function previous_page(): void
    {
        if (current_page_index > 0)
            current_page_index--;
        refresh(current_page_index);
    }

    export function next_page(): void
    {
        if (current_page_index < Number.MAX_VALUE)
		    current_page_index++;
	    refresh(current_page_index);
    }

    export function sort(field: HTMLTableHeaderCellElement): void
    {
        current_page_index = 0;

        if (!field.abbr.includes(current_sort)) {
            current_sort = field.abbr;
            current_sort_dir = sort_dir.ASC;
        } else {
            current_sort_dir = (current_sort_dir == sort_dir.ASC) ? sort_dir.DESC : sort_dir.ASC;
        }

        button_style();

        apply_sort();

        refresh(current_page_index);
    }

    export function filter_list(filter: string | number | string[]): void
    {
        let i: number;

        patrons_filtered = [];
        current_page_index = 0;

        for (i = 0; i < patrons_json.length; i++) {
            if (patrons_json[i].Name.toLowerCase().includes(filter.toString().toLowerCase())
                || patrons_json[i].Email.toLowerCase().includes(filter.toString().toLowerCase()))
                patrons_filtered.push(patrons_json[i]);
        }

        apply_sort();

        refresh(current_page_index);
    }

    function button_style(): void
    {
        let i: number;
        let wrapper: HTMLTableHeaderCellElement;
        let button: HTMLParagraphElement;
        let buttons: HTMLCollection;
        let marker: HTMLSpanElement;

        marker = document.getElementById("patron-list-marker");
            if (marker != null)
                marker.remove();

        buttons = document.getElementsByClassName("patron-list-sort");

        for (i = 0; i < buttons.length; i++) {
            button = buttons[i] as HTMLParagraphElement;
            wrapper = button.parentElement as HTMLTableHeaderCellElement;

            if (wrapper.abbr.includes(current_sort)) {
                button.classList.remove("btn-light");
                button.classList.add("btn-dark");

                marker = document.createElement("span") as HTMLSpanElement;
                marker.id = "patron-list-marker";
                marker.classList.add("oi");

                if (current_sort_dir == sort_dir.ASC)
                    marker.classList.add("oi-arrow-thick-top");
                else
                    marker.classList.add("oi-arrow-thick-bottom");

                button.appendChild(marker);
            } else {
                button.classList.remove("btn-dark");
                button.classList.add("btn-light");
            }
        }
    }

    function apply_sort(): void
    {
        let sort_high: number;

        if (current_sort_dir == sort_dir.ASC)
            sort_high = 1;
        else
            sort_high = -1;

        switch (current_sort) {
            case "ID":
                patrons_filtered = patrons_filtered.sort((a, b) => (a.ID - b.ID > 0) ? sort_high : -sort_high);
                break;
            case "Name":
                patrons_filtered = patrons_filtered.sort((a, b) => (a.Name.toLowerCase() > b.Name.toLowerCase()) ? sort_high : -sort_high);
                break;
            case "Email":
                patrons_filtered = patrons_filtered.sort((a, b) => (a.Email.toLowerCase() > b.Email.toLowerCase()) ? sort_high : -sort_high);
                break;
            case "LastActivity":
                patrons_filtered = patrons_filtered.sort((a, b) => (a.LastActivity > b.LastActivity) ? sort_high : -sort_high);
                break;
            default:
                break;
        }

        return;
    }

    function refresh(page_index: number): void
    {
        const num_rows: number = 10;
        let i: number;
        let row_index: number = num_rows * page_index;
        let last_page: boolean = false;
        let row: HTMLTableRowElement;
        let ID: HTMLTableHeaderCellElement;
        let Name: HTMLTableDataCellElement;
        let Email: HTMLTableDataCellElement;
        let LastActivity: HTMLTableDataCellElement;
        let Name_Link: HTMLAnchorElement;
        let Email_Link: HTMLAnchorElement;
        let LastActivity_Date: Date;

        $("#patron_list tbody tr").remove();

        for (i = row_index; i < row_index + num_rows; i++) {
            if (i == patrons_filtered.length) {
                last_page = true;
                break;
            }

            row = document.createElement("TR") as HTMLTableRowElement;
            ID = document.createElement("TH") as HTMLTableHeaderCellElement;
            Name = document.createElement("TD") as HTMLTableDataCellElement;
            Name_Link = document.createElement("A") as HTMLAnchorElement;
            Email = document.createElement("TD") as HTMLTableDataCellElement;
            Email_Link = document.createElement("A") as HTMLAnchorElement;
            LastActivity = document.createElement("TD") as HTMLTableDataCellElement;

            ID.innerHTML = patrons_filtered[i].ID.toString();
            Name_Link.innerHTML = patrons_filtered[i].Name;
            Name_Link.href = "/profile/" + patrons_filtered[i].ID.toString();
            Email_Link.innerHTML = patrons_filtered[i].Email;
            Email_Link.href = "mailto:" + patrons_filtered[i].Email;
            LastActivity_Date = new Date(patrons_filtered[i].LastActivity * 1000);
            LastActivity.innerHTML = LastActivity_Date.toLocaleDateString();

            Name.appendChild(Name_Link);
            Email.appendChild(Email_Link);

            row.appendChild(ID);
            row.appendChild(Name);
            row.appendChild(Email);
            row.appendChild(LastActivity);

            $("#patron_list tbody").append(row);
        }

        if (current_page_index == 0)
            $("#prev-button").addClass("d-none");
        else
            $("#prev-button").removeClass("d-none");

        if (last_page)
            $("#next-button").addClass("d-none");
        else
            $("#next-button").removeClass("d-none");
    }
}