import * as $ from "jquery";
import "jquery-ui-bundle";
import "bootstrap";

export module transaction {
    enum sort_dir {
        ASC
        ,DESC
    };

    class transaction {
        id: number;
        patron_id: number;
        amount: number | string;
        description: string;
        type: number;
        method: number;
        created_at: number;
        created_by: number | string;
        record_status: number;
    }

    class transaction_method {
        id: number;
        description: string;
        created_at: number;
        created_by: number;
        modified_at: number;
        modified_by: number;
        record_status: number;
    }

    export class transaction_type {
        id: number;
        description: string;
        is_payout: boolean | string;
        patron_required: boolean | string;
        reason_required: boolean;
        minimum_amount: number;
        created_at: number;
        created_by: number;
        modified_at: number;
        modified_by: number;
        record_status: number;
    }

    class transaction_post_model {
        action: string;
        date: string;
        type: string;
        method: string;
        patron: string;
        amount: string;
        description: string;

        constructor() {}
    }

    class user {
        ID: string;
        display_name: string;
    }

    export let transaction_methods: transaction_method[];
    export let transaction_types: transaction_type[];
    export let transactions: transaction[];
    export let patrons: user[];
    export let profile_link: string;
    export let is_admin: string | boolean;

    let incoming_chart_div: HTMLDivElement;
    let outgoing_chart_div: HTMLDivElement;

    let table_div: HTMLDivElement;
    let table: HTMLTableElement;
    let table_prev_button: HTMLButtonElement;
    let table_next_button: HTMLButtonElement;

    let modal_form: HTMLDivElement;
    let modal_post_form: HTMLFormElement;
    let modal_close: HTMLButtonElement;
    let modal_date_field: HTMLInputElement;
    let modal_incoming_button: HTMLInputElement;
    let modal_outgoing_button: HTMLInputElement;
    let modal_type_select: HTMLSelectElement;
    let modal_type_alert: HTMLDivElement;
    let modal_method_select: HTMLSelectElement;
    let modal_method_alert: HTMLDivElement;
    let modal_patron_group: HTMLDivElement;
    let modal_patron_select: HTMLSelectElement;
    let modal_patron_alert: HTMLDivElement;
    let modal_amount_field: HTMLInputElement;
    let modal_amount_alert: HTMLDivElement;
    let modal_description_field: HTMLTextAreaElement;

    const select_default: string = "-1";
    const table_body_name: string = "transaction-table-body";

    const date_header_text: string = "Date";
    const type_header_text: string = "Type";
    const method_header_text: string = "Method";
    const direction_header_text: string = "Direction";
    const amount_header_text: string = "Amount";
    const desc_header_text: string = "Description";
    const librarian_header_text: string = "Librarian";
    const delete_header_text: string = "Delete?";

    const sort_marker_name: string = "transaction-sort-marker";

    let current_page_index: number;
    let current_sort_column: string;
    let current_sort_dir: sort_dir;
    let is_synced: boolean;

    let validation_alerts: Array<HTMLDivElement>;

    /**
     * Prepares the transaction page for display, internally sorts the type and method collections alphabetically
     * @param incoming_chart_id HTML element ID of incoming finances chart
     * @param outgoing_chart_id HTML element ID of outgoing finances chart
     * @param table_id HTML element ID of the transaction table
     * @param modal_id HTML element ID of the transaction entry modal
     */
    export function on_load(incoming_chart_id: string, outgoing_chart_id: string, table_id: string, modal_id: string): void
    {
        let main_element: HTMLElement;

        /* lets sort this here, alphabetical order takes precedence in reference data for now */
        transaction_types = transaction_types.sort((a, b) => { return a.description.localeCompare(b.description); });
        transaction_methods = transaction_methods.sort((a, b) => { return a.description.localeCompare(b.description); });

        validation_alerts = new Array<HTMLDivElement>();

        main_element = document.getElementById("main");
        incoming_chart_div = document.getElementById(incoming_chart_id) as HTMLDivElement;
        outgoing_chart_div = document.getElementById(outgoing_chart_id) as HTMLDivElement;
        table_div = document.getElementById(table_id) as HTMLDivElement;
        current_page_index = 0;
        current_sort_dir = sort_dir.DESC;
        is_synced = true;

        google.charts.load("current", { packages: "corechart" });
        google.charts.setOnLoadCallback(chart_load);

        table_load();

        create_modal(modal_id);
        main_element.appendChild(modal_form);
    }

    function get_chart_data_for_types(types: transaction_type[]): number[][]
    {
        const months: number = 12;

        let i: number;
        let j: number = 0;
        let k: number;

        let date: Date = new Date();
        let current_year: number;

        let bounds: number[] = new Array<number>(13);

        let total: number = 0;
        let totals: number[] = new Array<number>(types.length);
        let cumulative: number[] = new Array<number>(types.length);
        let year: number[][] = new Array<number[]>(months);
        let year_and_cumulative: number[][] = new Array<number[]>(months + 1);

        current_year = date.getFullYear();

        date = new Date(current_year + 1, 0, 1, 0, 0, 0, 0);
        bounds[j++] = date.getTime() / 1000;

        for (i = 0; i < types.length; i++)
            cumulative[i] = 0;

        for (i = 11; i >= 0; i--) {
            date = new Date(current_year, i, 0, 1, 0, 0, 0);
            bounds[j++] = date.getTime() / 1000;
        }

        for (i = 0; i < months; i++) {
            for (j = 0; j < types.length; j++) {
                for (k = 0; k < transactions.length; k++) {
                    if (transactions[k].created_at < bounds[i] && transactions[k].created_at > bounds[i + 1] && transactions[k].type == types[j].id)
                        total += +transactions[k].amount;
                    else if (transactions[k].created_at <= bounds[i + 1])
                        break;
                }
                cumulative[j] += total;
                totals[j] = total;
                total = 0;
            }
            year[i] = totals;
            totals = new Array<number>(types.length);
        }
        year.reverse();

        for (i = 0; i < months; i++)
            year_and_cumulative[i] = year[i];

        year_and_cumulative[months] = cumulative;

        return year_and_cumulative;
    }

    function chart_load(): void
    {
        const months: number = 12;
        const month_strings: string[] = [
            "Jan"
            ,"Feb"
            ,"Mar"
            ,"Apr"
            ,"May"
            ,"Jun"
            ,"Jul"
            ,"Aug"
            ,"Sep"
            ,"Oct"
            ,"Nov"
            ,"Dec"
        ];

        let i: number;
        let j: number;
        let k: number = 0;
        let incoming_types: transaction_type[] = new Array<transaction_type>();
        let outgoing_types: transaction_type[] = new Array<transaction_type>();
        let incoming_totals: number[][];
        let outgoing_totals: number[][];

        let header_row: (string | number)[];
        let chart_row: (string | number)[];
        let chart_array: (string | number)[][] = new Array<(string | number)[]>(14);

        let incoming_chart_data: google.visualization.DataTable;
        let incoming_chart: google.visualization.BarChart = new google.visualization.BarChart(incoming_chart_div);
        let outgoing_chart_data: google.visualization.DataTable;
        let outgoing_chart: google.visualization.BarChart = new google.visualization.BarChart(outgoing_chart_div);

        for (i = 0; i < transaction_types.length; i++) {
            if (transaction_types[i].is_payout == "1")
                outgoing_types.push(transaction_types[i]);
            else
                incoming_types.push(transaction_types[i]);
        }

        if (incoming_types.length > 0) {
            incoming_totals = get_chart_data_for_types(incoming_types);

            header_row = new Array<string | number>(incoming_types.length + 1);
            chart_row = new Array<string | number>(incoming_types.length + 1);
            header_row[0] = "Month";
            for (i = 0; i < incoming_types.length; i++)
                header_row[i + 1] = incoming_types[i].description;
            chart_array[k++] = header_row;

            for (i = 0; i < months; i++) {
                chart_row[0] = month_strings[i];

                for (j = 0; j < incoming_types.length; j++)
                    chart_row[j + 1] = incoming_totals[i][j];

                chart_array[k++] = chart_row;
                chart_row = new Array<string | number>(incoming_types.length + 1);
            }

            chart_row[0] = "Average";
            for (i = 0; i < incoming_types.length; i++)
                chart_row[i + 1] = incoming_totals[months][i] / months;

            chart_array[k++] = chart_row;

            incoming_chart_data = google.visualization.arrayToDataTable(chart_array);

            incoming_chart.draw(incoming_chart_data, {
                height: 400
                ,legend: {
                    position: "top"
                    ,maxLines: 3
                }
                ,bar: {
                    groupWidth: "75%"
                }
                ,isStacked: true
                ,title: "Incoming"
                ,hAxis: {
                    title: "$"
                }
                ,vAxis: {
                    title: "Month"
                }
            });

            incoming_chart_div.classList.add("d-none", "d-lg-block");
        }

        if (outgoing_types.length > 0) {
            k = 0;
            chart_array = new Array<(string | number)[]>(14);
            outgoing_totals = get_chart_data_for_types(outgoing_types);

            header_row = new Array<string | number>(outgoing_types.length + 1);
            chart_row = new Array<string | number>(outgoing_types.length + 1);
            header_row[0] = "Month";
            for (i = 0; i < outgoing_types.length; i++)
                header_row[i + 1] = outgoing_types[i].description;
            chart_array[k++] = header_row;

            for (i = 0; i < months; i++) {
                chart_row[0] = month_strings[i];

                for (j = 0; j < outgoing_types.length; j++) {
                    chart_row[j + 1] = outgoing_totals[i][j];
                }

                chart_array[k++] = chart_row;
                chart_row = new Array<string | number>(outgoing_types.length + 1);
            }

            chart_row[0] = "Average";
            for (i = 0; i < outgoing_types.length; i++)
                chart_row[i + 1] = outgoing_totals[months][i] / months;

            chart_array[k++] = chart_row;

            outgoing_chart_data = google.visualization.arrayToDataTable(chart_array);

            outgoing_chart.draw(outgoing_chart_data, {
                height: 400
                ,legend: {
                    position: "top"
                    ,maxLines: 3
                }
                ,bar: {
                    groupWidth: "75%"
                }
                ,isStacked: true
                ,title: "Outgoing"
                ,hAxis: {
                    title: "$"
                }
                ,vAxis: {
                    title: "Month"
                }
            });

            outgoing_chart_div.classList.add("d-none", "d-lg-block");
        }
    }

    function table_load(): void
    {
        let colGroup: HTMLTableColElement;
            let dateCol: HTMLTableColElement;
            let typeCol: HTMLTableColElement;
            let methodCol: HTMLTableColElement;
            let dirCol: HTMLTableColElement;
            let amountCol: HTMLTableColElement;
            let descCol: HTMLTableColElement;
            let userCol: HTMLTableColElement;
            let deleteCol: HTMLTableColElement;

        let headerRow: HTMLTableRowElement;
            let dateHeader: HTMLTableHeaderCellElement;
                let dateHeaderButton: HTMLButtonElement;
            let typeHeader: HTMLTableHeaderCellElement;
                let typeHeaderButton: HTMLButtonElement;
            let methodHeader: HTMLTableHeaderCellElement;
                let methodHeaderButton: HTMLButtonElement;
            let dirHeader: HTMLTableHeaderCellElement;
            let amountHeader: HTMLTableHeaderCellElement;
                let amountHeaderButton: HTMLButtonElement;
            let descHeader: HTMLTableHeaderCellElement;
            let userHeader: HTMLTableHeaderCellElement;
            let deleteHeader: HTMLTableHeaderCellElement;

            let sortMarker: HTMLSpanElement;

        let buttonsDiv: HTMLDivElement;
            let prevButtonDiv: HTMLDivElement;
            let nextButtonDiv: HTMLDivElement;

        table = document.createElement("table") as HTMLTableElement;
            table.classList.add("table", "table-sm", "table-responsive-md", "table-hover");
            table_div.appendChild(table);

            colGroup = document.createElement("colgroup") as HTMLTableColElement;
                table.appendChild(colGroup);

                dateCol = document.createElement("col") as HTMLTableColElement;
                    dateCol.id = "date-column";
                    dateCol.classList.add("col-2");
                    colGroup.appendChild(dateCol);

                typeCol = document.createElement("col") as HTMLTableColElement;
                    typeCol.id = "type-column";
                    typeCol.classList.add("col-1");
                    colGroup.appendChild(typeCol);

                methodCol = document.createElement("col") as HTMLTableColElement;
                    methodCol.id = "method-column";
                    methodCol.classList.add("col-1");
                    colGroup.appendChild(methodCol);

                dirCol = document.createElement("col") as HTMLTableColElement;
                    dirCol.id = "direction-column";
                    dirCol.classList.add("col-1");
                    colGroup.appendChild(dirCol);

                amountCol = document.createElement("col") as HTMLTableColElement;
                    amountCol.id = "amount-column";
                    amountCol.classList.add("col-1");
                    colGroup.appendChild(amountCol);

                descCol = document.createElement("col") as HTMLTableColElement;
                    descCol.id = "description-column";
                    descCol.classList.add("col-3");
                    colGroup.appendChild(descCol);

                userCol = document.createElement("col") as HTMLTableColElement;
                    userCol.id = "user-column";
                    userCol.classList.add("col-2");
                    colGroup.appendChild(userCol);

                deleteCol = document.createElement("col") as HTMLTableColElement;
                    deleteCol.id = "delete-column";
                    deleteCol.classList.add("col-1");
                    colGroup.appendChild(deleteCol);
            
            table.tHead = table.createTHead();
                table.tHead.classList.add("thead-light");

            headerRow = document.createElement("tr") as HTMLTableRowElement;
                table.tHead.appendChild(headerRow);

            dateHeader = document.createElement("th") as HTMLTableHeaderCellElement;
                headerRow.appendChild(dateHeader);

                dateHeaderButton = document.createElement("button") as HTMLButtonElement;
                    dateHeaderButton.classList.add("btn", "btn-sm", "btn-dark", "mb-0");
                    dateHeaderButton.innerText = current_sort_column = date_header_text;
                    dateHeaderButton.onclick = table_sort;
                    dateHeader.appendChild(dateHeaderButton);

            typeHeader = document.createElement("th") as HTMLTableHeaderCellElement;
                headerRow.appendChild(typeHeader);

                typeHeaderButton = document.createElement("button") as HTMLButtonElement;
                    typeHeaderButton.classList.add("btn", "btn-sm", "btn-light", "mb-0");
                    typeHeaderButton.innerText = type_header_text;
                    typeHeaderButton.onclick = table_sort;
                    typeHeader.appendChild(typeHeaderButton);

            methodHeader = document.createElement("th") as HTMLTableHeaderCellElement;
                headerRow.appendChild(methodHeader);

                methodHeaderButton = document.createElement("button") as HTMLButtonElement;
                    methodHeaderButton.classList.add("btn", "btn-sm", "btn-light", "mb-0");
                    methodHeaderButton.innerText = method_header_text;
                    methodHeaderButton.onclick = table_sort;
                    methodHeader.appendChild(methodHeaderButton);

            dirHeader = document.createElement("th") as HTMLTableHeaderCellElement;
                dirHeader.innerText = direction_header_text;
                dirHeader.onclick = table_sort;
                headerRow.appendChild(dirHeader);

            amountHeader = document.createElement("th") as HTMLTableHeaderCellElement;
                headerRow.appendChild(amountHeader);

                amountHeaderButton = document.createElement("button") as HTMLButtonElement;
                    amountHeaderButton.classList.add("btn", "btn-sm", "btn-light", "mb-0");
                    amountHeaderButton.innerText = amount_header_text;
                    amountHeaderButton.onclick = table_sort;
                    amountHeader.appendChild(amountHeaderButton);

            descHeader = document.createElement("th") as HTMLTableHeaderCellElement;
                descHeader.innerText = desc_header_text;
                descHeader.onclick = table_sort;
                headerRow.appendChild(descHeader);

            userHeader = document.createElement("th") as HTMLTableHeaderCellElement;
                userHeader.innerText = librarian_header_text;
                userHeader.onclick = table_sort;
                headerRow.appendChild(userHeader);

            deleteHeader = document.createElement("th") as HTMLTableHeaderCellElement;
                deleteHeader.innerText = delete_header_text;
                deleteHeader.onclick = table_sort;
                headerRow.appendChild(deleteHeader);

                sortMarker = document.createElement("span") as HTMLSpanElement;
                    sortMarker.id = sort_marker_name;
                    sortMarker.classList.add("oi");
                    if (current_sort_dir == sort_dir.ASC)
                        sortMarker.classList.add("oi-arrow-thick-top");
                    else
                        sortMarker.classList.add("oi-arrow-thick-bottom");
                    dateHeaderButton.appendChild(sortMarker);

        buttonsDiv = document.createElement("div") as HTMLDivElement;
            buttonsDiv.classList.add("row");
            table_div.appendChild(buttonsDiv);

            prevButtonDiv = document.createElement("div") as HTMLDivElement;
                prevButtonDiv.classList.add("col-6");
                buttonsDiv.appendChild(prevButtonDiv);

                table_prev_button = document.createElement("button") as HTMLButtonElement;
                    table_prev_button.classList.add("btn", "btn-primary", "btn-sm", "d-none");
                    table_prev_button.innerText = "Previous";
                    table_prev_button.onclick = previous_page;
                    prevButtonDiv.appendChild(table_prev_button);

            nextButtonDiv = document.createElement("div") as HTMLDivElement;
                nextButtonDiv.classList.add("col-6");
                buttonsDiv.appendChild(nextButtonDiv);

                table_next_button = document.createElement("button") as HTMLButtonElement;
                    table_next_button.classList.add("btn", "btn-primary", "btn-sm", "float-right");
                    table_next_button.innerText = "Next";
                    table_next_button.onclick = next_page;
                    nextButtonDiv.appendChild(table_next_button);

        table_refill();
    }

    function table_sort(ev: MouseEvent): void
    {
        let i: number;
        let target: HTMLTableHeaderCellElement;
        let previous_target: HTMLTableHeaderCellElement;
        let sortMarker: HTMLSpanElement;
        let descending = false;
        const invalid_targets: string[] = [
            direction_header_text
            ,desc_header_text
            ,librarian_header_text
            ,delete_header_text
        ];

        target = ev.currentTarget as HTMLTableHeaderCellElement;

        for (i = 0; i < invalid_targets.length; i++)
            if (invalid_targets[i] === target.innerText)
                return;

        if (target.innerText != current_sort_column) {
            current_sort_column = target.innerText;
            current_sort_dir = sort_dir.DESC;
        } else {
            current_sort_dir = current_sort_dir == sort_dir.DESC ? sort_dir.ASC : sort_dir.DESC;
        }

        if (current_sort_dir != sort_dir.ASC)
            descending = true;

        sortMarker = document.getElementById(sort_marker_name) as HTMLSpanElement;

        if (sortMarker)
            sortMarker.remove();

        sortMarker = document.createElement("span") as HTMLSpanElement;
            sortMarker.id = sort_marker_name;
            sortMarker.classList.add("oi");
            if (current_sort_dir == sort_dir.ASC)
                sortMarker.classList.add("oi-arrow-thick-top");
            else
                sortMarker.classList.add("oi-arrow-thick-bottom");
            target.appendChild(sortMarker);

        if (target.classList.contains("btn-light")) {
            previous_target = document.getElementsByClassName("btn-dark")[0] as HTMLTableHeaderCellElement;
            previous_target.classList.remove("btn-dark");
            previous_target.classList.add("btn-light");

            target.classList.remove("btn-light");
            target.classList.add("btn-dark");
        }

        switch (current_sort_column) {
            case date_header_text:
                transactions = transactions.sort((a, b) => a.created_at - b.created_at);
                break;
            case type_header_text:
                transactions = sort_by_type(transactions);
                break;
            case method_header_text:
                transactions = sort_by_method(transactions);
                break;
            case amount_header_text:
                transactions = transactions.sort((a, b) => +a.amount - +b.amount);
                break;
            default:
                break;
        }

        if (descending)
            transactions = transactions.reverse();

        table_refill();
    }

    /**
     * Sorts a transaction list by type name alphabetically
     * @param transactions the list to sort
     * @returns the sorted list
     */
    function sort_by_type(transactions: transaction[]): transaction[]
    {
        let i: number;
        const retval: transaction[] = [];

        for (i = 0; i < transaction_types.length; i++) {
            retval.push(...transactions.filter(transaction => transaction.type == transaction_types[i].id));
        }

        return retval;
    }

    /**
     * Sorts a transaction list by method name alphabetically
     * @param transactions the list to sort
     * @returns the sorted list
     */
    function sort_by_method(transactions: transaction[]): transaction[]
    {
        let i: number;
        const retval: transaction[] = [];

        for (i = 0; i < transaction_methods.length; i++) {
            retval.push(...transactions.filter(transaction => transaction.method == transaction_methods[i].id));
        }

        return retval;
    }

    function table_refill(): void
    {
        let i: number;
        let j: number;
        let row_count: number = 10;
        let tbodies: ChildNode[] = new Array<ChildNode>();
        let tbody: HTMLTableSectionElement;
        let row: HTMLTableRowElement;
            let date: HTMLTableDataCellElement;
            let type: HTMLTableDataCellElement;
            let method: HTMLTableDataCellElement;
            let dir: HTMLTableDataCellElement;
            let amount: HTMLTableDataCellElement;
            let desc: HTMLTableDataCellElement;
                let desc_link: HTMLAnchorElement;
                let desc_text: string;
            let user: HTMLTableDataCellElement;
                let userLink: HTMLAnchorElement;
            let deleteD: HTMLTableDataCellElement;
                let deleteButton: HTMLButtonElement;

        let type_data: transaction_type;
        let method_data: transaction_method;
        let user_data: user;
        let row_index: number = current_page_index * 10;
        let last_page: boolean = false;

        tbody = document.getElementById(table_body_name) as HTMLTableSectionElement;

        if (tbody != null)
            tbody.remove();

        tbody = document.createElement("tbody") as HTMLTableSectionElement;
            tbody.id = table_body_name;
            table.appendChild(tbody);

        if (transactions == null || transactions.length === 0) {
            table_next_button.classList.add("d-none");
            table_prev_button.classList.add("d-none");
            return;
        }

        for (i = row_index; i < row_count + row_index; i++) {
            type_data = transaction_types.filter((value: transaction_type) => {
                return value.id == transactions[i].type
            })[0];

            method_data = transaction_methods.filter((value: transaction_method) => {
                return value.id == transactions[i].method
            })[0];

            user_data = patrons.filter((value: user) => {
                return value.ID == transactions[i].created_by
            })[0];

            row = document.createElement("tr") as HTMLTableRowElement;
                tbody.appendChild(row);

                date = document.createElement("td") as HTMLTableDataCellElement;
                    date.innerText = dateToShortString(new Date(transactions[i].created_at * 1000));
                    row.appendChild(date);
                type = document.createElement("td") as HTMLTableDataCellElement;
                    if (type_data != null)
                        type.innerText = type_data.description;
                    else
                        type.innerText = "Unknown";
                    row.appendChild(type);
                method = document.createElement("td") as HTMLTableDataCellElement;
                    if (method_data != null)
                        method.innerText = method_data.description;
                    else
                        method.innerText = "Unknown";
                    row.appendChild(method);
                dir = document.createElement("td") as HTMLTableDataCellElement;
                    if (type_data != null)
                        dir.innerText = type_data.is_payout == "1" ? "Outgoing" : "Incoming";
                    else
                        dir.innerText = "Unknown";
                    row.appendChild(dir);
                amount = document.createElement("td") as HTMLTableDataCellElement;
                    amount.classList.add("text-right");
                    if (type_data != null && type_data.is_payout == "1")
                        amount.innerText += "-";
                    amount.innerText += " $";
                    amount.innerText += transactions[i].amount?.toString();
                    row.appendChild(amount);
                desc = document.createElement("td") as HTMLTableDataCellElement;
                    if (type_data.patron_required == true) {
                        desc_link = document.createElement("a") as HTMLAnchorElement;
                        
                            desc_link.href = profile_link + transactions[i].patron_id.toString();

                            for (j = 0; j < patrons.length; j++) {
                                if (patrons[j].ID == transactions[i].patron_id.toString()) {
                                    desc_link.innerText = patrons[j].display_name;
                                    break;
                                }
                            }

                        desc.appendChild(desc_link);
                    } else
                        desc.innerText = transactions[i].description == "0" ? "" : transactions[i].description;
                    row.appendChild(desc);
                user = document.createElement("td") as HTMLTableDataCellElement;
                    if (user_data != null) {
                        userLink = document.createElement("a") as HTMLAnchorElement;
                            userLink.innerText = user_data.display_name;
                            userLink.href = "/profile/" + user_data.ID;
                            user.appendChild(userLink);
                    }
                    else
                        user.innerText = "Unknown";
                    row.appendChild(user);
                deleteD = document.createElement("td") as HTMLTableDataCellElement;
                    row.appendChild(deleteD);

                    deleteButton = document.createElement("button") as HTMLButtonElement;
                        deleteButton.classList.add("btn", "btn-sm", "btn-danger");
                        deleteButton.id = transactions[i].id?.toString();
                        deleteButton.innerText = "Delete";
                        deleteButton.onclick = deleteButton_onclick;
                        deleteD.appendChild(deleteButton);

            if (i + 1 == transactions.length) {
                last_page = true;
                break;
            }
        }

        if (current_page_index == 0)
            table_prev_button.classList.add("d-none");
        else
            table_prev_button.classList.remove("d-none");

        if (last_page)
            table_next_button.classList.add("d-none");
        else
            table_next_button.classList.remove("d-none");
    }

    function deleteButton_onclick(ev: MouseEvent): void
    {
        const Http: XMLHttpRequest = new XMLHttpRequest();

        let deleteButton: HTMLButtonElement = ev.currentTarget as HTMLButtonElement;

        if (confirm("Are you sure you want to do that?")) {
            if (!is_admin) {
                alert("Too bad!");
                return;
            }
            Http.addEventListener("load", (ev: ProgressEvent<XMLHttpRequestEventTarget>) => {
                let responseTarget: XMLHttpRequest;

                responseTarget = ev.target as XMLHttpRequest;
    
                if (responseTarget.status == 401) {
                    console.log("ajax: delete_transaction failed: 401");
                } else {
                    transactions_reload();
                }
            });
            Http.open("POST", "/wp-admin/admin-ajax.php?action=delete_transaction&id=" + deleteButton.id, true);
            Http.send();
        }
    }

    export function previous_page(ev: MouseEvent): void
    {
        if (current_page_index > 0)
            current_page_index--;
        table_refill();
    }

    export function next_page(ev: MouseEvent): void
    {
        if (current_page_index < Number.MAX_VALUE)
		    current_page_index++;
	    table_refill();
    }

    export function transactions_reload(): void
    {
        const Http: XMLHttpRequest = new XMLHttpRequest();

        Http.addEventListener("load", (ev: ProgressEvent<XMLHttpRequestEventTarget>) => {
            let responseTarget: XMLHttpRequest;

            responseTarget = ev.target as XMLHttpRequest;

            if (responseTarget.status == 401) {
                console.log("ajax: get_transactions failed: 401");
            } else {
                transactions = JSON.parse(JSON.parse(responseTarget.responseText) as string) as transaction[];
                table_refill();
            }
        });
        Http.open("GET", "/wp-admin/admin-ajax.php?action=get_transactions", true);
        Http.send();
    }

    export function create_modal(modal_id: string): void
    {
        let dialog: HTMLDivElement;
        let content: HTMLDivElement;
        let header: HTMLDivElement;
            let title: HTMLHeadingElement;
        let modal_body: HTMLDivElement;
            let first_row: HTMLDivElement;
                let date_group: HTMLDivElement;
                    let date_label: HTMLLabelElement;
                        const date_field_name: string = "modal_date";
                let direction_group: HTMLDivElement;
                    let incoming_group: HTMLDivElement;
                        let incoming_label: HTMLLabelElement;
                            const incoming_button_name: string = "modal_incoming";
                    let outgoing_group: HTMLDivElement;
                        let outgoing_label: HTMLLabelElement;
                            const outgoing_button_name: string = "modal_outgoing";
            let type_group: HTMLDivElement;
                let type_label: HTMLLabelElement;
                let type_option: HTMLOptionElement;
                    const type_select_name: string = "modal_type";
            let method_group: HTMLDivElement;
                let method_label: HTMLLabelElement;
                let method_option: HTMLOptionElement;
                    const method_select_name: string = "modal_method";
                let patron_label: HTMLLabelElement;
                let patron_option: HTMLOptionElement;
                    const patron_select_name: string = "modal_patron";
            let amount_group: HTMLDivElement;
                let amount_label: HTMLLabelElement;
                    const amount_field_name: string = "modal_amount";
            let description_group: HTMLDivElement;
                let description_label: HTMLLabelElement;
                    const description_field_name: string = "modal_description";
            let action_value_field: HTMLInputElement;
                const action_value_field_name: string = "action";
            
        let footer: HTMLDivElement;
            let cancel: HTMLButtonElement;
            let create: HTMLButtonElement;

        modal_form = document.createElement("div") as HTMLDivElement;
            modal_form.id = modal_id;
            modal_form.classList.add("modal", "fade");
            modal_form.tabIndex = -1;

        dialog = document.createElement("div") as HTMLDivElement;
            dialog.classList.add("modal-dialog");
            modal_form.appendChild(dialog);

        content = document.createElement("div") as HTMLDivElement;
            content.classList.add("modal-content");
            dialog.appendChild(content);

        header = document.createElement("div") as HTMLDivElement;
            header.classList.add("modal-header");
            content.appendChild(header);

            title = document.createElement("h5") as HTMLHeadingElement;
                title.classList.add("modal-title");
                title.innerText = "Add Transaction";
                header.appendChild(title);

            modal_close = document.createElement("button") as HTMLButtonElement;
                modal_close.classList.add("close", "oi", "oi-x");
                modal_close.type = "button";
                modal_close.setAttribute("data-dismiss", "modal");
                header.appendChild(modal_close);

        modal_body = document.createElement("div") as HTMLDivElement;
            modal_body.classList.add("modal-body");
            content.appendChild(modal_body);

            modal_post_form = document.createElement("form") as HTMLFormElement;
                modal_post_form.action = "/wp-admin/admin-ajax.php";
                modal_post_form.method = "post";
                modal_body.appendChild(modal_post_form);

                first_row = document.createElement("div") as HTMLDivElement;
                    first_row.classList.add("row");
                    modal_post_form.appendChild(first_row);

                    date_group = document.createElement("div") as HTMLDivElement;
                        date_group.classList.add("form-group", "col-6");
                        first_row.appendChild(date_group);

                        date_label = document.createElement("label") as HTMLLabelElement;
                            date_label.innerText = "Date: ";
                            date_label.htmlFor = date_field_name;
                            date_group.appendChild(date_label);

                        modal_date_field = document.createElement("input") as HTMLInputElement;
                            modal_date_field.id = date_field_name;
                            modal_date_field.classList.add("form-control", "form-control-sm");
                            $(modal_date_field).datepicker();
                            modal_date_field.placeholder = "(now)";
                            date_group.appendChild(modal_date_field);

                    direction_group = document.createElement("div") as HTMLDivElement;
                        direction_group.classList.add("form-group", "col-6", "mt-3");
                        first_row.appendChild(direction_group);

                        incoming_group = document.createElement("div") as HTMLDivElement;
                            incoming_group.classList.add("form-check");
                            direction_group.appendChild(incoming_group);

                            modal_incoming_button = document.createElement("input") as HTMLInputElement;
                                modal_incoming_button.id = incoming_button_name;
                                modal_incoming_button.classList.add("form-check-input");
                                modal_incoming_button.type = "radio";
                                modal_incoming_button.checked = true;
                                modal_incoming_button.onclick = incoming_button_click;
                                incoming_group.appendChild(modal_incoming_button);

                            incoming_label = document.createElement("label") as HTMLLabelElement;
                                incoming_label.classList.add("form-check-label");
                                incoming_label.innerText = "Incoming";
                                incoming_label.htmlFor = incoming_button_name;
                                incoming_group.appendChild(incoming_label);

                        outgoing_group = document.createElement("div") as HTMLDivElement;
                            outgoing_group.classList.add("form-check");
                            direction_group.appendChild(outgoing_group);

                            modal_outgoing_button = document.createElement("input") as HTMLInputElement;
                                modal_outgoing_button.id = outgoing_button_name;
                                modal_outgoing_button.classList.add("form-check-input");
                                modal_outgoing_button.type = "radio";
                                modal_outgoing_button.checked = false;
                                modal_outgoing_button.onclick = outgoing_button_click;
                                outgoing_group.appendChild(modal_outgoing_button);

                            outgoing_label = document.createElement("label") as HTMLLabelElement;
                                outgoing_label.classList.add("form-check-label");
                                outgoing_label.innerText = "Outgoing";
                                outgoing_label.htmlFor = outgoing_button_name;
                                outgoing_group.appendChild(outgoing_label);

                type_group = document.createElement("div") as HTMLDivElement;
                    type_group.classList.add("form-group");
                    modal_post_form.appendChild(type_group);

                    type_label = document.createElement("label") as HTMLLabelElement;
                        type_label.innerText = "Type: ";
                        type_label.htmlFor = type_select_name;
                        type_group.appendChild(type_label);

                    modal_type_select = document.createElement("select") as HTMLSelectElement;
                        modal_type_select.id = type_select_name;
                        modal_type_select.classList.add("form-control", "form-control-sm");
                        modal_type_select.onblur = modal_type_select_onblur;
                        modal_type_select.onchange = modal_type_select_onchange;
                        type_group.appendChild(modal_type_select);

                        type_option = document.createElement("option") as HTMLOptionElement;
                            type_option.text = "Select a transaction type";
                            type_option.value = select_default;
                            modal_type_select.appendChild(type_option);

                    modal_type_alert = document.createElement("div") as HTMLDivElement;
                        modal_type_alert.classList.add("alert", "alert-danger", "d-none");
                        modal_type_alert.setAttribute("role", "alert");
                        type_group.appendChild(modal_type_alert);
                        validation_alerts.push(modal_type_alert);

                method_group = document.createElement("div") as HTMLDivElement;
                    method_group.classList.add("form-group");
                    modal_post_form.appendChild(method_group);

                    method_label = document.createElement("label") as HTMLLabelElement;
                        method_label.innerText = "Method: ";
                        method_label.htmlFor = method_select_name;
                        method_group.appendChild(method_label);

                    modal_method_select = document.createElement("select") as HTMLSelectElement;
                        modal_method_select.id = method_select_name;
                        modal_method_select.classList.add("form-control", "form-control-sm");
                        modal_method_select.onblur = modal_method_select_onblur;
                        method_group.appendChild(modal_method_select);

                        method_option = document.createElement("option") as HTMLOptionElement;
                            method_option.text = "Select a payment method";
                            method_option.value = select_default;
                            modal_method_select.appendChild(method_option);

                    modal_method_alert = document.createElement("div") as HTMLDivElement;
                        modal_method_alert.classList.add("alert", "alert-danger", "d-none");
                        modal_method_alert.setAttribute("role", "alert");
                        method_group.appendChild(modal_method_alert);
                        validation_alerts.push(modal_method_alert);

                modal_patron_group = document.createElement("div") as HTMLDivElement;
                    modal_patron_group.classList.add("form-group", "d-none");
                    modal_post_form.appendChild(modal_patron_group);

                    patron_label = document.createElement("label") as HTMLLabelElement;
                        patron_label.innerText = "Patron: ";
                        patron_label.htmlFor = patron_select_name;
                        modal_patron_group.appendChild(patron_label);

                    modal_patron_select = document.createElement("select") as HTMLSelectElement;
                        modal_patron_select.id = patron_select_name;
                        modal_patron_select.classList.add("form-control", "form-control-sm");
                        modal_patron_select.onblur = modal_patron_select_onblur;
                        modal_patron_group.appendChild(modal_patron_select);

                        patron_option = document.createElement("option") as HTMLOptionElement;
                            patron_option.text = "Select a patron";
                            patron_option.value = select_default;
                            modal_patron_select.appendChild(patron_option);

                    modal_patron_alert = document.createElement("div") as HTMLDivElement;
                        modal_patron_alert.classList.add("alert", "alert-danger", "d-none");
                        modal_patron_alert.setAttribute("role", "alert");
                        modal_patron_group.appendChild(modal_patron_alert);
                        validation_alerts.push(modal_patron_alert);

                amount_group = document.createElement("div") as HTMLDivElement;
                    amount_group.classList.add("form-group");
                    modal_post_form.appendChild(amount_group);

                    amount_label = document.createElement("label") as HTMLLabelElement;
                        amount_label.innerText = "Amount: ";
                        amount_label.htmlFor = amount_field_name;
                        amount_group.appendChild(amount_label);

                    modal_amount_field = document.createElement("input") as HTMLInputElement;
                        modal_amount_field.id = amount_field_name;
                        modal_amount_field.classList.add("form-control", "form-control-sm");
                        modal_amount_field.onblur = modal_amount_field_onblur;
                        amount_group.appendChild(modal_amount_field);

                    modal_amount_alert = document.createElement("div") as HTMLDivElement;
                        modal_amount_alert.classList.add("alert", "alert-danger", "d-none");
                        modal_amount_alert.setAttribute("role", "alert");
                        amount_group.appendChild(modal_amount_alert);
                        validation_alerts.push(modal_amount_alert);

                description_group = document.createElement("div") as HTMLDivElement;
                    description_group.classList.add("form-group");
                    modal_post_form.appendChild(description_group);

                    description_label = document.createElement("label") as HTMLLabelElement;
                        description_label.innerText = "Description: ";
                        description_label.htmlFor = description_field_name;
                        description_group.appendChild(description_label);

                    modal_description_field = document.createElement("textarea") as HTMLTextAreaElement;
                        modal_description_field.id = description_field_name;
                        modal_description_field.classList.add("form-control", "form-control-sm");
                        description_group.appendChild(modal_description_field);

                action_value_field = document.createElement("input") as HTMLInputElement;
                    action_value_field.id = action_value_field_name;
                    action_value_field.classList.add("d-none");
                    action_value_field.value = "create_transaction";
                    modal_post_form.appendChild(action_value_field);

        footer = document.createElement("div") as HTMLDivElement;
            footer.classList.add("modal-footer");
            content.appendChild(footer);

            cancel = document.createElement("button") as HTMLButtonElement;
                cancel.classList.add("btn", "btn-secondary", "btn-sm");
                cancel.type = "button";
                cancel.innerText = "Close";
                cancel.setAttribute("data-dismiss", "modal");
                footer.appendChild(cancel);

            create = document.createElement("button") as HTMLButtonElement;
                create.classList.add("btn", "btn-primary", "btn-sm");
                create.type = "button";
                create.innerText = "Add Transaction";
                create.onclick = modal_create_onclick;
                footer.appendChild(create);

        populate_types(false);
        populate_methods();
        populate_patrons();
    }

    function incoming_button_click(): void
    {
        modal_outgoing_button.checked = false;
        populate_types(false);
    }

    function outgoing_button_click(): void
    {
        modal_incoming_button.checked = false;
        populate_types(true);
    }

    function populate_types(is_payout: boolean): void
    {
        let select_option: HTMLOptionElement;

        Array.from(modal_type_select.children).forEach((element: HTMLOptionElement) => {
            if (element.value != select_default)
                element.remove();
            else {
                modal_type_select.value = element.value;
                element.selected = true;
            }
        });

        transaction_types.forEach((element: transaction_type) => {
            if (element.is_payout == is_payout) {
                select_option = document.createElement("option") as HTMLOptionElement;
                    select_option.text = element.description;
                    select_option.value = element.id.toString();
                    modal_type_select.appendChild(select_option);
            }
        });
    }

    function populate_methods(): void
    {
        let select_option: HTMLOptionElement;

        transaction_methods.forEach((element: transaction_method) => {
            select_option = document.createElement("option") as HTMLOptionElement;
                select_option.text = element.description;
                select_option.value = element.id.toString();
                modal_method_select.appendChild(select_option);
        });
    }

    function populate_patrons(): void
    {
        let select_option: HTMLOptionElement;

        patrons.forEach((element: user) => {
            select_option = document.createElement("option") as HTMLOptionElement;
                select_option.text = element.display_name;
                select_option.value = element.ID;
                modal_patron_select.appendChild(select_option);
        });
    }

    function modal_validate(): void
    {
        modal_type_select.focus();
        modal_type_select.blur();
        modal_method_select.focus();
        modal_method_select.blur();
        if (!modal_patron_group.classList.contains("d-none")) {
            modal_patron_select.focus();
            modal_patron_select.blur();
        }
        modal_amount_field.focus();
        modal_amount_field.blur();
    }

    function modal_create_onclick(): void
    {
        let i: number;
        let is_valid: boolean = true;

        modal_validate();

        for (i = 0; i < validation_alerts.length; i++)
            if (!validation_alerts[i].classList.contains("d-none")) {
                is_valid = false;
                break;
            }

        if (!is_valid)
            return;

        modal_ajax_post();
        modal_close.click();
        modal_reset();
    }

    function modal_ajax_post(): void
    {
        const Http: XMLHttpRequest = new XMLHttpRequest();

        let date: Date;
        let transaction_post: transaction_post_model = new transaction_post_model();
        
        transaction_post.action = "create_transaction";

        if (!modal_date_field.value) {
            transaction_post.date = modal_date_field.value;
        } else {
            date = new Date(modal_date_field.value);
            transaction_post.date = Math.floor(date.getTime() / 1000).toString();
        }

        transaction_post.type = modal_type_select.value;
        transaction_post.method = modal_method_select.value;
        transaction_post.patron = modal_patron_select.value;
        transaction_post.amount = modal_amount_field.value;
        transaction_post.description = modal_description_field.value;

        Http.addEventListener("load", (ev: ProgressEvent<XMLHttpRequestEventTarget>) => {
            let responseTarget: XMLHttpRequest;

            responseTarget = ev.target as XMLHttpRequest;

            if (responseTarget.status == 401) {
                console.log("ajax: create_transaction failed: 401");
            } else {
                current_page_index = 0;
                transactions_reload();
            }
        });
        Http.open("POST", "/wp-admin/admin-ajax.php?" + $.param(transaction_post), true);
        Http.send();
    }

    function modal_reset(): void
    {
        modal_date_field.value = null;
        modal_incoming_button.click();
        modal_type_select.value = select_default;
        modal_method_select.value = select_default;
        modal_patron_select.value = select_default;
        modal_amount_field.value = null;
        modal_description_field.value = null;

        validation_alerts.forEach((element: HTMLDivElement) => {
            element.classList.add("d-none");
        });

        modal_patron_group.classList.add("d-none");
    }

    function modal_type_select_onchange(ev: Event): void
    {
        let type: transaction_type = transaction_types.filter((value: transaction_type) => {
            return String(value.id) == modal_type_select.value
        })[0];

        if (type != null && type.patron_required == "1")
            modal_patron_group.classList.remove("d-none");
        else
            modal_patron_group.classList.add("d-none");
    }

    function modal_type_select_onblur(ev: FocusEvent): void
    {
        let target: HTMLSelectElement = ev.currentTarget as HTMLSelectElement;

        if (target.value == select_default) {
            modal_type_alert.classList.remove("d-none");
            modal_type_alert.innerText = "Please select a payment type."
        }
        else {
            modal_type_alert.classList.add("d-none");
            modal_amount_field_onblur(ev);
        }
    }

    function modal_method_select_onblur(ev: FocusEvent): void
    {
        let target: HTMLSelectElement = ev.currentTarget as HTMLSelectElement;

        if (target.value == select_default) {
            modal_method_alert.classList.remove("d-none");
            modal_method_alert.innerText = "Please select a payment method.";
        }
        else
            modal_method_alert.classList.add("d-none");
    }

    function modal_patron_select_onblur(ev: FocusEvent): void
    {
        let target: HTMLSelectElement = ev.currentTarget as HTMLSelectElement;

        if (target.value == select_default) {
            modal_patron_alert.classList.remove("d-none");
            modal_patron_alert.innerText = "Please select a patron for this transaction type.";
        }
        else
            modal_patron_alert.classList.add("d-none");
    }

    function modal_amount_field_onblur(ev: FocusEvent): void
    {
        let target: HTMLInputElement = modal_amount_field as HTMLInputElement;
        const type: transaction_type = transaction_types.filter((value: transaction_type) => {
            return String(value.id) == modal_type_select.value
        })[0];

        if (isNaN(+target.value)) {
            modal_amount_alert.classList.remove("d-none");
            modal_amount_alert.innerText = "Please enter a numeric amount.";
        } else if (type == null) {
            modal_amount_alert.classList.remove("d-none");
            modal_amount_alert.innerText = "Please select a valid payment type.";
        } else if (+target.value < type.minimum_amount) {
            modal_amount_alert.classList.remove("d-none");
            modal_amount_alert.innerText = "Please enter at least " + String(type.minimum_amount) + " dollars.";
        }
        else
            modal_amount_alert.classList.add("d-none");
    }

    function dateToShortString(date: Date): string
    {
        const year_digits: number = 2;

        let retval: string;
        let year: string;
        let hour: number;
        let am: boolean;

        year = date.getFullYear().toString();
        year = year.substring(year.length - year_digits, year.length);

        hour = date.getHours();
        am = hour < 12 ? true : false;
        hour = (hour = hour % 12) == 0 ? 12 : hour;

        retval = (date.getMonth() + 1) + "/" + date.getDate() + "/" + year + " " + hour + ":" + date.getMinutes().toLocaleString("en-US", { minimumIntegerDigits: 2, useGrouping:false }) + " " + (am ? "AM" : "PM");

        return retval;
    }
}
