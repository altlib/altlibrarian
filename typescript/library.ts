import * as $ from "jquery";

export module library {
    class subgenre {
        id: number;
        name: string;
    }
    export let subgenres: subgenre[][];
    export let patron_id: number;
    let book_id: number;

    export function navigate_to_genre(): void
    {
        let genreValue: string | number | string[] = $("#genreSelect").find(":selected").val();

        document.location.href = "https://" + document.location.host + "/browse/" + genreValue;
    }

    export function update_subgenre(sub: string | number | string[]): void
    {
        let sub_index: number;
        let genre_index: number;
        let genre_select : HTMLSelectElement;
        let option: HTMLOptionElement;
        let sub_default: HTMLOptionElement;

        genre_select = document.getElementById("library-genre") as HTMLSelectElement;
        genre_index = genre_select.selectedIndex;

        if (sub == "") {
            $("#library-subgenre .subgenre").remove();
            $("#library-subgenre").val(0);
            sub_default = document.getElementById("subgenre-default") as HTMLOptionElement;
            sub_default.selected = true;
        }

        if (genre_index <= 0) {
            $("#library-subgenre-group").addClass("d-none");
            return;
        } else {
            $("#library-subgenre-group").removeClass("d-none");
            genre_index--;
        }

        for (sub_index = 0; sub_index < subgenres[Number(genre_index)].length; sub_index++) {
            option = document.createElement("OPTION") as HTMLOptionElement;
            option.value = subgenres[Number(genre_index)][sub_index].id.toString();
            option.innerHTML = subgenres[Number(genre_index)][sub_index].name;
            option.classList.add("subgenre");
            if (sub != "" && option.value == sub)
                option.selected = true;
            $("#library-subgenre").append(option);
        }
    }

    export function create_check(title: string | number | string[]): void
    {
        $.post(
            "/wp-admin/admin-ajax.php"
            ,{
                action: "check_title"
                ,s: title
            }
            ,create
        );
    }

    export function integrate(item_id: number, title: string): void
    {
        if (confirm("You wanna add " + title + " to the shelves?")) {
            $("#integrate-form #ID").val(item_id);
            $("#integrate-form").trigger("submit");
        }
    }

    export function duplicate(): void
    {
        $("#post_parent").val($("#post_id").val());
        $("#library-form").trigger("submit");
    }

    export function mark_lost(): void
    {
        if (confirm("Mark book as lost?"))
            $("#lost-form").trigger("submit");
    }

    export function delete_book(id: number): void
    {
        if (confirm("Are you sure?")) {
            book_id = id;
            $.post(
                "/wp-admin/admin-post.php"
                ,{
                    action:	"delete_item"
                    ,ID:	id
                }
                ,delete_callback);
        }
    }

    export function check_out(id: number): void
    {
        $.post(
            "/wp-admin/admin-ajax.php"
            ,{
                action:     "check_out"
                ,patron_id: patron_id
                ,id:        id
            }
            ,check_update
        );
    }

    export function check_in(id: number): void
    {
        $.post(
            "/wp-admin/admin-ajax.php"
            ,{
                action:     "check_in"
                ,patron_id: patron_id
                ,id:        id
            }
            ,check_update
        );
    }

    function create(data: string | boolean): void
    {
        let genre_select : HTMLSelectElement;
        let genre_index: number;

        genre_select = document.getElementById("library-genre") as HTMLSelectElement;
        genre_index = genre_select.selectedIndex;

        if (data != false) {
            $("#notice").html(data + " already exists! Choose another title.");
            $("#notice").removeClass("d-none");
        } else {
            if (genre_index > 0) {
                $("#library-form").trigger("submit");
            } else {
                $("#notice").html("Genre can't be blank!");
                $("#notice").removeClass("d-none");
            }
        }
    }

    function delete_callback(data: string): void
    {
        if (data == "deleted") {
            console.log($("#copy_" + book_id));
            $("#copy_" + book_id).remove();
        } else {
            $("#notice").html("Error encountered deleting duplicate.");
            $("#notice").removeClass("d-none");
        }
    }

    function check_update(data: string): void
    {
        $("#preview").html("");
        $("#checked").html(data);
    }
}