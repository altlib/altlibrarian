import * as $ from "jquery";
import "bootstrap";

export module note {
    var current_item: number;

    export function create(item_id: number, patron_id: number): void
    {
        current_item = item_id;
        $("#outnotes-item_id").val(item_id);
        $("#outnotes-patron_id").val(patron_id);
        $("#myModal").modal();
    }
    export function save(): void
    {
        $.post("/wp-admin/admin-post.php", $("#note-form").serialize(), update_note);
        $("#myModal").modal("hide");
    }
    export function del_from(note_id: number, item_id: number): void
    {
        current_item = item_id;
        $.post(
            "/wp-admin/admin-post.php"
            ,{
                action: "delete_note"
                ,note_id: note_id
                ,item_id: item_id
            }
            ,update_note
        );
    }

    function update_note(data: string): void
    {
        $("#notes-" + current_item).html(data);
    }
}