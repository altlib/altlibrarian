import * as $ from "jquery";

export module request {
    export namespace vote {
        export function up(item_id: number): void
        {
            $("#vote-form #ID").val(item_id);
            $("#vote-form #action").val("vote_up");
            $("#vote-form").trigger("submit");
        }

        export function down(item_id: number): void
        {
            $("#vote-form #ID").val(item_id);
            $("#vote-form #action").val("vote_down");
            $("#vote-form").trigger("submit");
        }
    }
    export namespace manage {
        export class EmailDict {
            msg_id: number;
            type_dropdown: string;
        }
        
        export let email_dict: EmailDict[] = [
            { msg_id: 1, type_dropdown: "Thanks for participating in our request list, however this book is already part of our collection and available for check out. It’s currently checked in and can be placed on hold for you if you’d like."}
            ,{ msg_id: 2, type_dropdown: "Thanks for participating in our request list, however this book is already part of our collection, though it is currently checked out. We’ll add you to the hold list for this book and notify you once it is checked in. This book will also be listed for consideration in keeping duplicate copies for our collection." }
            ,{ msg_id: 3, type_dropdown: "Thanks for participating in our request list, however the book you requested has already been added to our request list by another patron. Please visit the library request list to cast your vote - https://altlib.org/requests-list/" }
        ];

        export function accept(item_id: number): void
        {
            document.location.href = "https://" + document.location.host + "/add-item/?ID=" + item_id;
        }

        export function decline(item_id: number): void
        {
            if ($("#decline_text_" + item_id).val() != "") {
                $("#decline-form #ID").val(item_id);
                $("#decline-form #content").val($("#decline_text_" + item_id).val());
                $("#decline-form").trigger("submit");
            } else {
                alert("Please type a message to the patron!");
            }
        }

        export function decline_prompt(item_id: number): void
        {
            $("#decline_box_" + item_id).removeClass("d-none");
            $("#accept_decline_buttons").hide();
        }

        export function decline_cancel(item_id: number): void
        {
            $("#decline_box_" + item_id).addClass("d-none");
            $("#accept_decline_buttons").show();
        }

        export function decline_reason(item_id: number, msg_id: number, type_dropdown: string): void
        {
            $("#decline_text_" + item_id).val(email_dict[msg_id - 1].type_dropdown);
        }
    }
}
