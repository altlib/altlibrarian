<?php
/**
 * Plugin Name:	Alt Librarian
 * Plugin URI:	http://altlib.org
 * Description:	A plugin that implements a web-based library catalog and circulation System
 * Version:		1.9.6
 * Author:		David Zhang, Matt Gilmore
 * Author URI:	http://altlib.org
 * License:		GPL v2 or later
 * License URI:	https://www.gnu.org/licenses/gpl-2.0.html
 */

/*
 * This plugin establishes the following:
 * 
 * Roles:
 * 	Editor -> Librarian
 * 	Subscriber -> Patron
 * 
 * Capabilities:
 * 	Administrator:
 * 		+manage_patrons
 * 		+manage_collection
 * 		+manage_circulation
 * 	Librarian:
 * 		+manage_patrons
 * 		+manage_collection
 * 		+manage_circulation
 */
require_once(plugin_dir_path(__FILE__)."AltLibrarian_Repo.php");

if (!class_exists("AltLibrarian")) {
	class TransactionViewModel
	{
		public $can_manage_circulation;

		public $transaction_methods;
		public $transaction_types;
		public $transactions;
		public $patrons;
		public $profile_link;
		public $is_admin;
		public $incoming_chart_id;
		public $outgoing_chart_id;
		public $modal_id;
		public $table_id;
	}

	class TransactionPostModel
	{
		public $date;
		public $type;
		public $method;
		public $patron;
		public $amount;
		public $description;
	}

	class AltLibrarian
	{
		private $version = "1.9.6";
		private $repo;

		// This is bad design but the quickest way I could implement
		// Set this to "true" to enable the transaction entry to substitute $5
		// of every type = 7 transaction as a type = 1 transaction, posting the
		// remaining to type 7. In context, types 1 and 7 are "Membership" and
		// "CSA" at The Alternative Library and the current rule is the first $5
		// of any CSA payment go towards Membership instead. Eventually this should
		// be a trigger/stored procedure in the database.
		private $csa_hack = true;

		// Currently the concept of paying a patron's dues is contingent on
		// presence of at least a single payment type and method
		// This should be replaced with a plugin option to denote the patron
		// payment type, and paying patrons should give an option of selecting a
		// payment method. For now, Bellingham Alternative Library is using
		// a combination of "Membership" type and "Other" method
		private $patron_payment_hack = true;
		
		public $templates;

		/* constructor - perform injections */
		function __construct()
		{
			register_activation_hook(__FILE__, array($this, "activate"));
			register_deactivation_hook(__FILE__, array($this, "deactivate"));
			
			define("WEBLIB_BASEURL", plugins_url("", __FILE__));
			define("WEBLIB_CSSURL", plugins_url("/css", __FILE__));
			define("WEBLIB_JSURL", plugins_url("/js", __FILE__));
			define("WEBLIB_IMAGEURL", plugins_url("/images", __FILE__));

			/* ensure options exist */
			if (!get_option("altlib_email_address"))
				add_option("altlib_email_address");
			if (!get_option("altlib_email_signature"))
				add_option("altlib_email_signature");
			if (!get_option("altlib_name_short"))
				add_option("altlib_name_short");
			if (!get_option("altlib_name_long"))
				add_option("altlib_name_long");

			/* init */
			add_action("init", array($this, "init_action"));

			/* hide the admin bar */
			add_filter("show_admin_bar", "__return_false");
			
			/* update check */
			add_action("plugins_loaded", array($this, "update_db_check"));

			/* custom script additions */
			add_action("wp_print_scripts", array($this, "add_front_scripts"));
			
			/* custom theme template substitutions */
			add_filter("archive_template", array($this, "get_custom_archive_template"));
			add_filter("author_template", array($this, "get_custom_author_template"));
			add_filter("single_template", array($this, "get_custom_single_template"));
			add_filter("search_template", array($this, "get_custom_search_template"));
			
			/* inject ajax calls */
			add_action("wp_ajax_list_items", array($this, "ajax_list_items"));
			add_action("wp_ajax_check_out", array($this, "ajax_check_out"));
			add_action("wp_ajax_check_in", array($this, "ajax_check_in"));
			add_action("wp_ajax_checked_items", array($this, "ajax_checked_items"));
			add_action("wp_ajax_display_item", array($this, "ajax_display_item"));

			add_action("wp_ajax_transactions_by_month_by_type", array($this, "transactions_ajax_by_month_by_type"));
			add_action("wp_ajax_transactions_get", array($this, "transactions_ajax_get"));
			
			add_action("wp_ajax_check_title", array($this, "ajax_check_title"));
			
			add_action("wp_ajax_outlist", array($this, "ajax_outlist"));

			add_action("wp_ajax_create_transaction", array($this, "ajax_create_transaction"));
			add_action("wp_ajax_get_transactions", array($this, "ajax_get_transactions"));
			add_action("wp_ajax_delete_transaction", array($this, "ajax_delete_transaction"));
			
			/* inject posts */
			add_action("admin_post_save_note", array($this, "post_save_note"));
			add_action("admin_post_delete_note", array($this, "post_delete_note"));

			add_action("admin_post_pay_patron", array($this, "post_pay_patron"));

			add_action("admin_post_vote_up", array($this, "post_vote_up"));
			add_action("admin_post_vote_down", array($this, "post_vote_down"));
			add_action("admin_post_vote_for_them", array($this, "post_vote_for_them"));
			
			add_action('admin_post_add_money', array($this, 'add_money'));
			add_action('admin_post_delete_transaction', array($this, 'delete_transaction'));

			add_action('admin_post_edit_item', array($this, 'edit_item'));
			add_action('admin_post_delete_item', array($this, 'delete_item'));

			add_action('admin_post_edit_user', array($this, 'edit_user'));
			add_action('admin_post_edit_profile', array($this, 'edit_profile'));

			add_action('admin_post_make_request', array($this, 'make_request'));
			add_action('admin_post_confirm_request', array($this, 'confirm_request'));
			add_action('admin_post_integrate_request', array($this, 'integrate_request'));
			add_action('admin_post_mark_as_lost', array($this, 'mark_as_lost'));

			/* admin settings */
			add_action("admin_post_admin_update_email", array($this, "admin_update_email"));
			add_action("admin_post_admin_update_branding", array($this, "admin_update_branding"));

			add_filter('wp_nav_menu_items', array($this, 'add_login_out_item_to_menu'), 50, 2 );

			add_action('pre_get_posts', array($this, 'post_id_sort'));
			add_filter('pre_get_posts',array($this, 'search_filter'), 0);

			add_filter('login_redirect', array($this, 'my_login_redirect'), 10, 3 );
			add_filter('login_message', array($this, 'the_login_message'));

			add_filter('wp_mail_from', array($this, 'wpb_sender_email'));
			add_filter('wp_mail_from_name', array($this, 'wpb_sender_name'));

			/* admin panels */
			add_action("admin_menu", array($this, "admin_menu_init"));
			
			$this->plugin_locale = 'pte';

			/* inject templates */
			if (version_compare(floatval(get_bloginfo("version")), "4.7", "<"))
				add_filter("page_attributes_dropdown_pages_args", array($this, "register_project_templates"));
			else
				add_filter("theme_page_templates", array($this, "add_new_template"));
			add_filter("wp_insert_post_data", array($this, "register_project_templates"));
			add_filter("template_include", array($this, "view_project_template"));
			
			$this->templates = array(
				"browse.php"			=> __("Browse", "alt-librarian")
				,"out-list.php"			=> __("Out List", "alt-librarian")
				,"requests-list.php"	=> __("Requests List", "alt-librarian")
				,"requests-manage.php"	=> __("Requests Manage", "alt-librarian")
				,"lost.php"				=> __("Lost", "alt-librarian")
				,"form-item.php"        => __("Item Form", "alt-librarian")
				,"form-user.php"        => __("User Form", "alt-librarian")
				,"form-profile.php"     => __("Profile Form", "alt-librarian")
				,"form-request.php"     => __("Request Form", "alt-librarian")
				,"form-voteforem.php"   => __("Vote For Them Form", "alt-librarian")
				,"reviews.php"          => __("Reviews", "alt-librarian")
				,"transactions.php"     => __("Transactions", "alt-librarian")
				,"restricted.php"       => __("Restricted Page", "alt-librarian")
				,"patron-list.php"		=> __("Patron List", "alt-librarian")
				,"testing.php"          => __("TESTING (do not use)", "alt-librarian"));

			$this->repo = new AltLibrarian_Repo();
		}
		
		/* activate - activate plugin */
		function activate()
		{
			$role = get_role("administrator");
				$role->add_cap("manage_patrons");
				$role->add_cap("manage_collection");
				$role->add_cap("manage_circulation");

			$role = get_role("editor");
				$role->add_cap("manage_patrons");
				$role->add_cap("manage_collection");
				$role->add_cap("manage_circulation");

			add_option("altlib_email_address");
			add_option("altlib_email_signature");
			add_option("altlib_name_short");
			add_option("altlib_name_long");

			$this->update_db_check();
		}

		/* deactivate - remove custom capabilities */
		function deactivate()
		{	
			delete_option("altlib_email_address");
			delete_option("altlib_email_signature");
			delete_option("altlib_name_short");
			delete_option("altlib_name_long");

			$role = get_role("administrator");
				$role->remove_cap("manage_patrons");
				$role->remove_cap("manage_collection");
				$role->remove_cap("manage_circulation");

			$role = get_role("editor");
				$role->remove_cap("manage_patrons");
				$role->remove_cap("manage_collection");
				$role->remove_cap("manage_circulation");
		}

		/* update_db_check - check for an updated database version */
		function update_db_check()
		{
			$plugin_ver = get_option("altlib_db_version");

			if ($plugin_ver == false)
				add_option("altlib_db_version", $this->version);
			else if ($plugin_ver != $this->version)
				update_option("altlib_db_version", $this->version);
			else
				return;

			$this->repo->db_create();
		}

		/* init_admin_menu - create backpanel administrative menus */
		function admin_menu_init()
		{
			add_menu_page(
				"AltLibrarian",
				"AltLibrarian",
				"manage_patrons",
				"altlib_admin_menu",
				array($this, "admin_menu"),
				"dashicons-book-alt",
				null
			);
			add_submenu_page(
				"altlib_admin_menu",
				"Email",
				"Email",
				"manage_patrons",
				"altlib_admin_email",
				array($this, "admin_menu_email"),
				null
			);
			add_submenu_page(
				"altlib_admin_menu",
				"Branding",
				"Branding",
				"manage_patrons",
				"altlib_admin_branding",
				array($this, "admin_menu_branding"),
				null
			);
		}

		function admin_menu()
		{
			$this->admin_menu_email();
		}

		/* admin_menu_email - emailer text and addresses */
		function admin_menu_email()
		{
			include plugin_dir_path(__FILE__)."templates/admin/email.php";
		}

		/* admin_get_email - retrieve the email string */
		function admin_get_email()
		{
			return get_option("altlib_email_address");
		}

		/* admin_get_emailSig - retrieve the email signature string */
		function admin_get_emailSig()
		{
			return get_option("altlib_email_signature");;
		}

		/* admin_update_email - update email text and addresses in database */
		function admin_update_email()
		{
			if (is_admin()) {
				update_option("altlib_email_address", $_REQUEST["address"]);
				update_option("altlib_email_signature", $_REQUEST["signature"]);
			}

			wp_safe_redirect(admin_url("admin.php?page=altlib_admin_email"));
		}

		/* admin_menu_branding - branding for "named" areas */
		function admin_menu_branding()
		{
			include plugin_dir_path(__FILE__)."templates/admin/branding.php";
		}

		/* admin_get_shortName - get the short name of the org */
		function admin_get_shortName()
		{
			return get_option("altlib_name_short");
		}

		/* admin_get_longName - get the long name of the org */
		function admin_get_longName()
		{
			return get_option("altlib_name_long");
		}

		/* admin_update_branding - update the names of the org */
		function admin_update_branding()
		{
			if (is_admin()) {
				update_option("altlib_name_short", $_REQUEST["shortName"]);
				update_option("altlib_name_long", $_REQUEST["longName"]);
			}

			wp_safe_redirect(admin_url("admin.php?page=altlib_admin_branding"));
		}
		
		/* init_action - initialize the plugin */
		function init_action()
		{
			global $wp_roles;
			
			/* setup roles */
			if (!isset($wp_roles))
				$wp_roles = new WP_Roles();
			
			$wp_roles->roles["editor"]["name"] = "Librarian";
			$wp_roles->role_names["editor"] = "Librarian";
			$wp_roles->roles["author"]["name"] = "Author";
			$wp_roles->role_names["author"] = "Author";
			$wp_roles->roles["subscriber"]["name"] = "Patron";
			$wp_roles->role_names["subscriber"] = "Patron";
						
			//add_option("maplib_settings", array());
			
			/* setup post types */
			$args = array(
				"public"		=> true
				,"query_var"	=> "item"
				,"rewrite"		=> array(
					"slug"			=> 'item'
					,"with_front"	=> false)
				,"supports"		=> array(
					"title"
					,"editor"
					,"thumbnail"
					,"comments")
				,"labels"		=> array(
					"name"				=> "Library"
					,"singular_name"	=> "Item"
					,"add_new"			=> "Add New Item"
					,"add_new_item"		=> "Add New Item"
					,"edit_item"		=> "Edit Item"
					,"new_item"			=> "New Item"
					,"view_item"		=> "View Item"
					,"search_items"		=> "Search Items"
					,"not_found"		=> "No Items Found"
					,"not_found_in_trash"	=> "No Items Found In Trash"));
			register_post_type("item", $args);

			$genre_args = array(
				"hierarchical"		=> true
				,"query_var"		=> "item_genre"
				,"show_tagcloud"	=> true
				,"rewrite"			=> array(
					"slug"				=> "browse"
					,"with_front"		=> false
					,"hierarchical"		=> true)
				,"labels"			=> array(
					"name"				=> "Genres"
					,"singular_name"	=> "Genre"
					,"edit_item"		=> "Edit Genre"
					,"update_item"		=> "Update Genre"
					,"add_new_item"		=> "Add New Genre"
					,"new_item_name"	=> "New Genre Name"
					,"all_items"		=> "All Genres"
					,"search_items"		=> "Search Genres"
					,"parent_item"		=> "Parent Genre"
					,"parent_item_colon" => "Parent Genre:"));
			register_taxonomy("item_genre", "item", $genre_args);

			/* setup styles and scripts */
			wp_enqueue_style("altlib-bootstrap", WEBLIB_CSSURL."/bootstrap.min.css");
			
			wp_enqueue_style("altlib-oi", WEBLIB_CSSURL."/open-iconic.min.css");
			wp_enqueue_style("altlib-bootstrap-oi", WEBLIB_CSSURL."/open-iconic-bootstrap.min.css", array("altlib-bootstrap", "altlib-oi"));
			
			wp_enqueue_style("altlib-datepicker", WEBLIB_CSSURL."/jquery-ui.min.css");
			
			/* setup rewrites */
			add_rewrite_rule("browse/page/?([0-9]{1,})/*$", "index.php?page_id=20000&paged=\$matches[1]", "top");
		}

		/* add_front_scripts - add additional scripts for display */
		function add_front_scripts()
		{			
			wp_enqueue_script("altlib-popper", WEBLIB_JSURL."/popper.min.js", array("jquery"));
			wp_enqueue_script("altlib-bootstrap", WEBLIB_JSURL."/bootstrap.min.js", array("jquery", "altlib-popper"));
			
			wp_enqueue_script("altlib-typeahead", WEBLIB_JSURL."/typeahead.bundle.min.js", array("jquery"));
			
			wp_enqueue_script("altlib-datepicker", WEBLIB_JSURL."/jquery-ui.min.js", array("jquery"));
			
			wp_enqueue_script("google-chart", "https://www.gstatic.com/charts/loader.js");
			
			wp_enqueue_script("altlib-front", WEBLIB_JSURL."/front.min.js");
				wp_localize_script("altlib-front", "altlib_front", $this->localize_vars_front());
		}

		/* localize_vars_front - provide text domain substitutions for script variables */
		function localize_vars_front()
		{
			return array(
				"WEBLIB_BASEURL"	=> WEBLIB_BASEURL
				,"hold"				=> __("Hold", "alt-librarian")
				,"holds"			=> __("Holds", "alt-librarian")
				,"nodata"			=> __("Ajax error:  No Data Received", "alt-librarian")
				,"ajaxerr"			=> __("Ajax error: ", "alt-librarian")
			);
		}
		
		/* get_custom_archive_template - replace the theme's archive template */
		function get_custom_archive_template()
		{
			global $post;
		
			return $post != null ? $post->post_type == "item" ? dirname(__FILE__)."/templates/browse.php" : null : null;
		}
		
		/* get_custom_author_template - replace the theme's author template */
		function get_custom_author_template()
		{
			return dirname(__FILE__)."/templates/author.php";
		}

		/* get_custom_search_template - replace the theme's search template */
		function get_custom_search_template()
		{
			return dirname(__FILE__)."/templates/search.php";
		}

		/* get_custom_single_template - replace the theme's single-page template */
		function get_custom_single_template()
		{
			global $post;
			
			return $post->post_type == "item" ? dirname(__FILE__)."/templates/single-item.php" : null;
		}

		/* register_project_templates - add templates to cache */
		function register_project_templates($atts)
		{
			// Create the key used for the themes cache
			$cache_key = "page_templates-".md5(get_theme_root()."/".get_stylesheet());
			// Retrieve the cache list. If it doesn't exist, or it's empty prepare an array
			$templates = wp_get_theme()->get_page_templates();
			if (empty($templates))
				$templates = array();
			// Since we've updated the cache, we need to delete the old cache
			wp_cache_delete($cache_key , "themes");
			// Now add our template to the list of templates by merging our templates
			// with the existing templates array from the cache.
			$templates = array_merge($templates, $this->templates);
			// Add the modified cache to allow WordPress to pick it up for listing
			// available templates
			wp_cache_add($cache_key, $templates, "themes", 1800);
			return $atts;
		}
		
		/* add_new_template - adds a new template to cache */
		function add_new_template($template)
		{
			$template = array_merge($template, $this->templates);
			return $template;
		}
		
		/* view_project_template - check if template is assigned */
		function view_project_template($template)
		{
			global $post;
			// If no posts found, return to
			// avoid "Trying to get property of non-object" error
			if (!$post)
				return $template;
			if (!isset($this->templates[get_post_meta($post->ID, "_wp_page_template", true)]))
				return $template;
			$file = plugin_dir_path(__FILE__)."templates/".get_post_meta($post->ID, "_wp_page_template", true);
			// Just to be safe, we check if the file exist first
			if (file_exists($file))
				return $file;
			
			return $template;
		}

		/* wpb_sender_email - sender address for emails */
		function wpb_sender_email($original_email_address)
		{
			return $this->admin_get_email();
		}

		/* wpb_sender_name - sender name for emails */
		function wpb_sender_name($original_email_from)
		{
			return $this->admin_get_longName();
		}

		/* the_login_message - prints the message displayed at the top of the login screen */
		function the_login_message($message)
		{
			if (empty($message)) {
				return "<p class='message'>Log in to your patron account using the e-mail that you gave us when you set up your patron account to see what you have checked out, write book reviews, or vote on the request list. <br><br> If you are visiting the site for the first time, you may need to use the 'Lost your password' function to create a new password.</p>";
			} else {
				return $message;
			}
		}

		/* add_login_out_item_to_menu - add a logout item to the menu */
		function add_login_out_item_to_menu($items, $args)
		{
			//change theme location with your them location name
			if(is_admin() || $args->theme_location != "primary")
				return $items;

			$redirect = is_home() ? false : get_permalink();

			$items = preg_replace("/http:\/\/\w+\.*\w*\.*\w*\/profile/",  site_url("profile/".get_current_user_id()), $items);
			$items = preg_replace("/http:\/\/\w+\.*\w*\.*\w*\/wp-login\.php\?action=logout/", wp_logout_url($redirect), $items);
			return $items;
		}

		/* my_login_redirect - replace login redirect url */
		function my_login_redirect($redirect_to, $request, $user)
		{
			//is there a user to check?
			global $user;

			// always go back to home page
			return home_url();

			// code below is skipped, but could be used in the future
			if ( isset( $user->roles ) && is_array( $user->roles ) ) {
				//check for admins
				if ( in_array( 'administrator', $user->roles ) ) {
					// redirect them to the default place
					return $redirect_to;
				} else {
					return home_url();
				}
			} else {
				return home_url();
			}
		}

		function ajax_get_transactions()
		{
			if (!current_user_can("manage_circulation")) {
				status_header("401");
				echo "Not authenticated.";
				wp_die();
			}

			$results = $this->transactions_get_all();

			echo json_encode($results);

			wp_die();
		}

		function ajax_delete_transaction()
		{
			if (!current_user_can("manage_circulation")) {
				status_header("401");
				echo "Not authenticated.";
				wp_die();
			}

			$id = $_REQUEST["id"];

			$this->repo->transaction_delete($id);

			wp_die();
		}

		function ajax_create_transaction()
		{
			if (!current_user_can("manage_circulation")) {
				status_header("401");
				echo "Not authenticated.";
				wp_die();
			}

			$model = new TransactionPostModel();

			$model->date = $_REQUEST["date"];
			$model->type = $_REQUEST["type"];
			$model->method = $_REQUEST["method"];
			$model->patron = $_REQUEST["patron"];
			$model->amount = $_REQUEST["amount"];
			$model->description = $_REQUEST["description"];

			// Set $csa_hack true for this, type_id 7 = CSA, type_id 1 = Membership
			// Current Alternative Library rule to stand up first $5 of any
			// CSA payment as a Membership payment instead
			if ($this->csa_hack == true && $model->type == 7) {
				$membership_csa_amount = 5;
				$membership_transaction = new Transaction();
				
				$membership_transaction->patron_id = $model->patron;
				$membership_transaction->amount = $membership_csa_amount;
				$membership_transaction->description = $model->description;
				$membership_transaction->type = 1;
				$membership_transaction->method = $model->method;

				if ($model->date == "")
					$membership_transaction->created_at = time();
				else
					$membership_transaction->created_at = $model->date;
				$membership_transaction->created_by = get_current_user_id();

				$model->amount = $model->amount - $membership_csa_amount;

				$this->repo->transaction_create($membership_transaction);
			}

			$transaction = new Transaction();

			$transaction->patron_id = $model->patron;
			$transaction->amount = $model->amount;
			$transaction->description = $model->description;
			$transaction->type = $model->type;
			$transaction->method = $model->method;
			if ($model->date == "")
				$transaction->created_at = time();
			else
				$transaction->created_at = $model->date;
			$transaction->created_by = get_current_user_id();

			$this->repo->transaction_create($transaction);

			wp_die();
		}

		/* ajax_list_items - perform a search for book lookup */
		function ajax_list_items()
		{
			$results = array();

			if (isset($_REQUEST["term"])) {
				$term = strtolower(sanitize_text_field($_REQUEST["term"]));
				$matches = array();

				$exact = $this->repo->post_get_by_title_prefix($term);

				foreach ($exact as $book)
					array_push($matches, $book->ID);

				$fuzzy = $this->repo->post_get_by_title_contains($term);

				foreach ($fuzzy as $book) {
					if (!in_array($book->ID, $matches))
						array_push($matches, $book->ID);
				}
				
				$books = $this->repo->post_get_for_search($matches, "in");

				foreach ($books as $book)
					array_push($results, array("id" => $book->ID, "value" => $book->post_title));
			}

			echo json_encode($results);

			wp_die();
		}
		
		/* ajax_check_out - check out a book */
		function ajax_check_out()
		{
			if (isset($_REQUEST["id"]) && isset($_REQUEST["patron_id"])) {
				update_post_meta($_REQUEST["id"], "cf_outdate", time());
				update_post_meta($_REQUEST["id"], "cf_status", $_REQUEST["patron_id"]);

				update_user_meta($_REQUEST["patron_id"], "last_activity", time());

				$this->activity_log_record("check_out", $_REQUEST["id"], get_the_title($_REQUEST["id"]));
			}

			$this->ajax_checked_items();
		}
		
		/* ajax_check_in - check in a book */
		function ajax_check_in()
		{
			if (isset($_REQUEST["id"]) && isset($_REQUEST["patron_id"])) {
				$pop = intval(get_post_meta($_REQUEST["id"], "cf_popularity", true));
				update_post_meta($_REQUEST["id"], "cf_popularity", ++$pop);
				update_post_meta($_REQUEST["id"], "cf_outdate", time());
				update_post_meta($_REQUEST["id"], "cf_status",  "in");

				update_user_meta($_REQUEST["patron_id"], "last_activity", time());
					
				$this->repo->out_note_delete_by_item($_REQUEST["id"]);

				$this->activity_log_record("check_in", $_REQUEST["id"], get_the_title($_REQUEST["id"]));
			}
			
			$this->ajax_checked_items();
		}
		
		/* ajax_checked_items - retrieve the list of checked out books */
		function ajax_checked_items($patron_id = null)
		{
			if (isset($_REQUEST["patron_id"]))
				$patron_id = $_REQUEST["patron_id"];
			
			$args = array(
				"post_type"		=> "item"
				,"orderby"		=> "title"
				,"nopaging"		=> true
				,"order"		=> "ASC"
				,"meta_query"	=> array(array(
					"key"			=> "cf_status"
					,"value"		=> $patron_id
					,"compare"		=> "=")));
					
			$results = new WP_Query($args);
			while ($results->have_posts()) {
				$results->the_post();
				$this->get_item(true);
				if (current_user_can("manage_circulation")) {
?>
					<div id="notes-<?php echo get_the_ID();?>">
						<?php $this->get_out_notes(get_the_ID()); ?>
					</div>
					<a class="btn btn-sm btn-primary" onclick='altlib.note.create(<?php echo get_the_ID();?>, <?php echo get_post_meta(get_the_ID(), "cf_status", true); ?>)'> Add Note</a>
<?php
				}
			}
			if (isset($_REQUEST["patron_id"]))
				wp_die();
		}
		
		/* ajax_voted_items - get a list of votes */
		function ajax_voted_items($patron_id = null)
		{
			if (isset($_REQUEST["patron_id"]))
				$patron_id = $_REQUEST["patron_id"];
			
			$args = array(
				"post_type"		=> "item"
				,"orderby"		=> "title"
				,"nopaging"		=> true
				,"order"		=> "ASC"
				,"meta_query"	=> array(array(
					"key"			=> "cf_status"
					,"value"		=> "req"
					,"compare"		=> "=")));

			$votes = $this->repo->vote_get_ids_by_patron($patron_id);

			$results = new WP_Query($args);
			
			while ($results->have_posts()) {
				$results->the_post();
				if (!in_array(get_the_ID(), $votes))
					continue;
				$this->get_item(true);
			}
			
			if (isset($_REQUEST["patron_id"]))
				wp_die();
		}
		
		/* ajax_display_item - show an item */
		function ajax_display_item()
		{
			$args = array(
				"p"				=> $_REQUEST["id"]
				,"post_type"	=> "item");
			$result = new WP_Query($args);
			$result->the_post();
			$this->get_item(true);
			wp_die();
		}
		
		/* ajax_check_title - check if title exists */
		function ajax_check_title()
		{
			$searchfield = sanitize_title($_REQUEST["s"]);

			$books = $this->repo->post_get_by_name($searchfield);

			if (count($books) > 0)
				echo $books[0]->post_title;
			else
				echo false;

			wp_die();
		}
		
		/* ajax_outlist - outlist */
		function ajax_outlist()
		{
			$query_result = $this->repo->post_get_by_meta_with_meta("cf_status", $_REQUEST["patron_id"]);
			
			$results = array(
				"membership_fee"	=> $this->get_last_pay(intval($_REQUEST["patron_id"]))
				,"books"			=> array()
			);

			foreach ($query_result as $book) {
				array_push($results["books"], array(
					"id"		=>	$book->ID
					,"title"	=>	$book->post_title
					,"author"	=>	$book->PostMetas["cf_author"]
					,"outdate"	=>	$book->PostMetas["cf_outdate"]
				));
			}

			echo json_encode($results);

			wp_die();
		}
		
		/* save_note - post a note to a book */
		function post_save_note()
		{
			$note = $_POST["OutNotes"];

			$out_note = new Out_Note();
			$out_note->item_id = $note["item_id"];
			$out_note->patron_id = $note["patron_id"];
			$out_note->comments = $note["comments"];
			$out_note->color = $note["color"];
			$out_note->created_at = time();
			
			$this->repo->out_note_create($out_note);
			
			$this->get_out_notes($note["item_id"]);
		}

		/* delete_note - delete a note from a book */
		function post_delete_note()
		{
			$this->repo->out_note_delete($_POST["note_id"]);
			
			$this->get_out_notes($_POST["item_id"]);
		}
		
		/* pay_patron - pay into a patron account */
		function post_pay_patron()
		{
			if (!isset($_POST["ID"])
				|| !isset($_POST["amount"]))
				return false;

			$patron_id = $_POST["ID"];

			// For now, patron dues are hacked in, replace these with the IDs of the methods and
			// types for patron accounts
			$transaction_method_id = 0;
			$transaction_type_id = 0;
			if ($this->patron_payment_hack) {
				$transaction_method_id = $this->repo->transaction_method_get_id_by_desc("Other");
				$transaction_type_id = $this->repo->transaction_type_get_id_by_desc("Membership");
			}

			$transaction = new Transaction();
			$transaction->patron_id = $patron_id;
			$transaction->amount = $_POST["amount"];
			$transaction->description = null;
			$transaction->type = $transaction_type_id;
			$transaction->method = $transaction_method_id;
			$transaction->created_at = time();
			$transaction->created_by = get_current_user_id();

			$this->repo->transaction_create($transaction);
			$this->repo->user_set_active($patron_id);
			
			$this->activity_log_record("patron_pay", $patron_id, $_POST["amount"]);

			wp_safe_redirect("/profile/".$patron_id);
		}

		/* vote_up - add a vote to a book */
		function post_vote_up()
		{
			$vote = new Vote();
			$vote->timestamp = time();
			$vote->patron_id = get_current_user_id();
			$vote->item_id = $_POST["ID"];

			$this->repo->vote_create($vote);

			$votes = get_post_meta($_REQUEST["ID"], "cf_popularity", true);
			update_post_meta($_REQUEST["ID"], "cf_popularity", ++$votes);

			wp_safe_redirect("/requests-list");
		}

		/* vote_down - remove a vote from a book */
		function post_vote_down()
		{
			$this->repo->vote_delete_by_item_patron($_POST["ID"], $_POST["patron_id"]);

			$votes = get_post_meta($_REQUEST["ID"], "cf_popularity", true);
			$votes--;
			if ($votes == 0)
				wp_delete_post($_REQUEST["ID"], true);
			else
				update_post_meta($_REQUEST["ID"], "cf_popularity", $votes);

			wp_safe_redirect("/requests-list");
		}

		/* vote_for_them - cast a vote for a user */
		function post_vote_for_them()
		{
			$vote = new Vote();

			$vote->timestamp = time();
			$vote->patron_id = $_POST["user_id"];
			$vote->item_id = $_POST["ID"];

			$this->repo->vote_create($vote);

			$votes = get_post_meta($_REQUEST["ID"], "cf_popularity", true);
			update_post_meta( $_REQUEST["ID"], "cf_popularity", ++$votes);

			wp_safe_redirect("/requests-list");
		}
		
		/* edit_item - either save edits or create a new book, adding a vote for a new request */
		function edit_item()
		{
			$item = $_POST["Library"];
			$book = new Post();

			foreach ($item as $index=>$field) {
				$item[$index] = stripcslashes($field);
			}
			
			$meta_fields = array(
				"author",
				"publisher",
				"pubdate",
				"image",
				"status",
				"popularity",
				"last",
			);
			
			$request_fields = array(
				"author",
				"status",
				"popularity",
				"last",
			);
			
			if (!empty($item["ID"]) && ($item["ID"] != $item["post_parent"])) {
				$book->ID = $item["ID"];
				$book->post_content = $item["description"];
				$book->post_title = $item["title"];

				$post_id = $this->repo->post_update($book);
			} else {
				$book->post_content = $item["description"];
				$book->post_title = $item["title"];
				$book->post_parent = $item["post_parent"] == $item["ID"] ? $item["post_parent"] : 0;

				$post_id = $this->repo->post_create($book);
			}

			if (!$item["status"])
				$item["status"] = "in";

			if ($item["status"] != "unconfirmed") {
				$cat_ids = array();
				foreach ($meta_fields as $field) {
					if($field == "status" && $item["ID"] == $item["post_parent"])
						$item[$field] = "in";
					
					if (!add_post_meta($post_id, "cf_".$field, $item[$field], true))
						update_post_meta($post_id, "cf_".$field, $item[$field]);
				}

				if ($item["genre"])
					$cat_ids[] = intval($item["genre"]);
				
				if (isset($item["subgenre"]))
					$cat_ids[] = intval($item["subgenre"]);

				wp_set_object_terms($post_id, $cat_ids, "item_genre");
			} else {
				$vote = new Vote();
				$vote->timestamp = time();
				$vote->patron_id = get_current_user_id();
				$vote->item_id = $post_id;

				$this->repo->vote_create($vote);

				foreach ($request_fields as $field) {
					if ($field == "last" && !isset($item["last"]))
						continue;
					if (!add_post_meta($post_id, "cf_".$field, $item[$field], true))
						update_post_meta($post_id, "cf_".$field, $item[$field]);
				}
			}

			$url = get_permalink($post_id);
			if ($url)
				wp_redirect($url);
		}

		/* edit_user - edit or create a patron account */
		function edit_user()
		{
			// For now, patron dues are hacked in, replace these with the IDs of the methods and
			// types for patron accounts
			$transaction_method_id = 0;
			$transaction_type_id = 0;
			if ($this->patron_payment_hack) {
				$transaction_method_id = $this->repo->transaction_method_get_id_by_desc("Other");
				$transaction_type_id = $this->repo->transaction_type_get_id_by_desc("Membership");
			}
			$membership_fee = $_POST["membership_fee"];

			if ($_POST["ID"]) {
				$patron_id = $_POST["ID"];
				$dues = $this->get_membership_fee($patron_id);
			} else {
				$user_login = $_POST["user_email"];
				$user_email = $_POST["user_email"];
				$patron_id = register_new_user($user_login, $user_email);
				if (is_wp_error($patron_id)) {
					print_r($patron_id);
					exit();
				} else {
					$user = new WP_User($patron_id);

					$transaction = new Transaction();
					$transaction->patron_id = $patron_id;
					$transaction->amount = $membership_fee;
					$transaction->description = null;
					$transaction->type = $transaction_type_id;
					$transaction->method = $transaction_method_id;
					$transaction->created_at = time();
					$transaction->created_by = get_current_user_id();

					$this->repo->transaction_create($transaction);

					wp_new_user_notification($patron_id, null, "user");
				}
				$dues = $membership_fee;
			}

			$meta_values = array(
				"first_name"		=> $_POST["first_name"]
				,"last_name"		=> $_POST["last_name"]
				,"nickname"		=> $_POST["first_name"]." ".$_POST["last_name"]
				,"phone"		=> $_POST["phone"]
				,"notes"		=> $_POST["notes"]
				,"last_activity"	=> time()
			);

			if ($membership_fee > $dues) {
				// Paid by an increase on the edit page
				$transaction = new Transaction();
				$transaction->patron_id = $patron_id;
				$transaction->amount = $membership_fee - $dues;
				$transaction->description = null;
				$transaction->type = $transaction_type_id;
				$transaction->method = $transaction_method_id;
				$transaction->created_at = time();
				$transaction->created_by = get_current_user_id();

				$this->repo->transaction_create($transaction);
			}

			foreach ($meta_values as $key=>$value) {
				update_user_meta($patron_id, $key, $value);
			}

			wp_update_user(array(
				"ID"			=> $patron_id
				,"user_email"	=> $_POST["user_email"]
				,"user_login"	=> $_POST["user_email"]
				,"display_name"	=> $meta_values["nickname"]
			));

			$nicename = get_user_meta($patron_id, "user_nicename", true);

			if ($nicename != $patron_id)
				wp_update_user(array(
					"ID"				=> $patron_id
					,"user_nicename"	=> $patron_id
				));

			wp_safe_redirect("/profile/".$patron_id);
		}

		/* edit_profile - edit patron profile data */
		function edit_profile()
		{
			$user_id = get_current_user_id();
			$meta_values = array(
				"nickname"		=> $_POST["nickname"]
				,"description"	=> $_POST["notes"]
			);

			foreach ($meta_values as $key=>$value) {
				update_user_meta($user_id, $key, $value);
			}
			wp_update_user(array(
				"ID" => 			$user_id
				,"display_name"	=>	$meta_values["nickname"]
			));

			wp_safe_redirect("/profile/".$user_id);
		}
		
		/* get_patrons - get list for Patron List display */
		function get_patrons()
		{
			return $this->repo->user_get_patron_list();
		}
	
		/* get_item - display an item record */
		function get_item($author = null)
		{
			$args = array(
				'parent' => 0,
				'post_id' => get_the_ID(),

			);
			$comments = get_comments( $args );
			$total = 0;
			foreach ( $comments as $comment ) {
				$current_rating  = $comment->comment_karma;
				$total = $total + (double) $current_rating;
			}
			if(count($comments) > 0)
				$average = $total / count( $comments );
			else
				$average = 0;
?>
			<div class="row px-3 my-2">
				<div class="col-2 px-0">
					<a class="" href="<?php the_permalink(); ?>">
						<img class="img-fluid" src="<?php echo get_post_meta(get_the_ID(), "cf_image", true); ?>">
					</a>
				</div>
				<div class="col-10">
					<h4><a href="<?php the_permalink(); ?>"><?php echo the_title(); ?></a></h4>
					<b>Author: </b><?php echo $this->author_link(get_post_meta(get_the_ID(), "cf_author", true)); ?>
					<br clear="all">
					<?php $this->get_genre(); ?>
					<br clear="all">
					<div class="review_rate small" data-pixrating="<?php echo $average; ?>"></div>
<?php
					$stat = get_post_meta(get_the_ID(), "cf_status", true);
					if (current_user_can("manage_circulation") && $author && $stat != "req") {
						if ($stat == "in")
							echo "<a href='javascript:void(0)' class='btn btn-primary btn-sm' onclick='altlib.library.check_out(".get_the_ID().")'>Check Out</a>";
						else
							echo "<a href='javascript:void(0)' class='btn btn-primary btn-sm' onclick='altlib.library.check_in(".get_the_ID().")'>Check In</a>";
					} else
						echo $this->get_status(get_the_ID());
?>
				</div>
			</div>
<?php
		}

		/* get_status - get the status of each copy of a book */
		function get_status($id)
		{
			/* TODO: There is a known bug in here concerning lost books */
			$book = $this->repo->post_get_by_id($id);
			$copies = $this->repo->post_get_ids_by_title($book->post_title);

			$status = "";
			$count = 0;
			asort($copies);

			foreach ($copies as $copy_id) {
				$item_status = get_post_meta($copy_id, "cf_status", true);
				$out_date = intval(get_post_meta($copy_id, "cf_outdate", true));
				$out_date = date_i18n(get_option("date_format"), $out_date);

				if ($item_status != "req" && $item_status != "lost") {
					$count++;
					if ($item_status == "in") {
						$status .= "Copy #".$count.": in<br>";
					} else if (is_numeric($item_status)) {
						if (count($copies) > 1) {
							$status .= "Copy #".$count.": checked out";
							if (current_user_can("manage_patrons"))
								$status .= " to <a href='/profile/".$item_status."'>".get_userdata($item_status)->display_name."</a> on ".$out_date;
							$status .= "<br>";
						} else {
							$status = "Copy #1: checked out";
							if (current_user_can("manage_patrons"))
								$status .= " to <a href='/profile/".$item_status."'>".get_userdata($item_status)->display_name."</a> on ".$out_date;
							$status .= "<br>";
						}
					} else {
						$status = $item_status;
					}
				} else if ($item_status == "req") {
					$status = "Requested";
				} else {
					$status = "Lost";
				}
			}

			return $status;
		}

		/* get_genre - display a multi-part genre string with links */
		function get_genre()
		{
			$display = array();
			$terms = wp_get_post_terms(get_the_ID(), "item_genre");
			foreach ($terms as $term) {
				if($term->parent == 0)
					$display[0] = $term;
				else
					$display[1] = $term;
			}

			if (isset($display[0])) {
?>
				<a href="/browse/<?php echo $display[0]->slug; ?>"><?php echo $display[0]->name; ?></a>
<?php
				if (isset($display[1])) {
?>
					» <a href="/browse/<?php echo $display[0]->slug."/".$display[1]->slug; ?>"><?php echo $display[1]->name; ?></a>
<?php
				}
			} else {
				echo "Genre not set";
			}
		}
		
		/* get_out_notes - display notes */
		function get_out_notes($item_id)
		{
			$notes = $this->repo->out_note_get_by_item($item_id);
			
			foreach ($notes as $note) {
				echo "
				<div class='small' onmouseover='$(\"#delete_".$note->id."\").show()' onmouseout='$(\"#delete_".$note->id."\").hide()' style='padding:5px 0;margin:0;display:block;color:".$note->color."'>
					".human_time_diff($note->created_at)." ago<br>
					".$note->comments."<a href='javascript:void(0)' onclick='altlib.note.del_from(".$note->id.", ".$note->item_id.")'><i id='delete_".$note->id."' class='oi oi-x' style='display:none'></i></a>
				</div>";
			}
		}
		
		/* get_request - draw a book request record */
		function get_request($number, $available_votes)
		{
			$all_votes = get_post_meta(get_the_ID(), "cf_popularity", true);
			
			$user_votes = $this->repo->vote_get_count_for_item_patron(get_the_ID(), get_current_user_id());
?>
			<div class="row">
				<div class="col-1 text-center p-1">
<?php
					if (is_user_logged_in() && $user_votes < 2 && $available_votes > 0) {
?>
						<a href="javascript:altlib.request.vote.up(<?php echo get_the_ID(); ?>)"><span class="oi oi-arrow-top"></span></a>
<?php
					}
?>
					<br/>
					<span class="badge badge-pill badge-success"><?php echo $all_votes; ?></span>
					<br/>
<?php
					if (is_user_logged_in() && $user_votes > 0) {
?>
						<a href="javascript:altlib.request.vote.down(<?php echo get_the_ID(); ?>)"><span class="oi oi-arrow-bottom"></span></a>
<?php
					}
?>
					<br/>
					<h3><small><?php echo $number; ?></small></h3>
				</div>
				<div class="col-2 col-lg-1 p-1">
					<img class="img-fluid" src="<?php echo get_post_meta( get_the_ID(), "cf_image", true ); ?>">
				</div>
				<div class="col-9 col-lg-10 p-1">
					<h4><a href="<?php the_permalink(); ?>"><?php echo the_title(); ?></a></h4>
					<b>Author: </b><?php echo $this->author_link(get_post_meta(get_the_ID(), "cf_author", true)); ?>
					<br/>
					<?php $this->get_genre(); ?>
					<br/>
				</div>
			</div>
<?php
		}

		/* get_reviews_count - get the total number of reviews */
		function get_reviews_count()
		{
			return $this->repo->comments_get_count();
		}

		/* get_reviews - get a set of reviews */
		function get_reviews($index, $count)
		{
			return $this->repo->comments_get_subset($index, $count);
		}

		/* get_requested_books - get a list of requests */
		function get_requested_books()
		{
			return $this->repo->post_get_by_meta("cf_status", "req");
		}

		/* get_book_copies - get copies of a book */
		function get_book_copies($book_id)
		{
			return $this->repo->post_get_ids_by_parent($book_id);
		}

		/* author_link - display a link to the authors */
		function author_link($authors)
		{
			$authors = preg_split("/,/", trim(preg_replace("/\\s*\\([^)]+\\)/", "", $authors)));
			foreach ($authors as &$author) {
				$author = preg_replace("/\\&\\s*/", "", $author);
				$author = "<a href='/?s=".$author."'>$author</a>";
			}
			return implode(", ", $authors);
		}

		/* genre_dropdown - provide a dropdown genre menu */
		function genre_dropdown($genres, $genre)
		{
?>
			<div class="col-12 d-block d-lg-none">
				<select id="genreSelect" class="form-control" empty="(select a genre)" onchange="altlib.library.navigate_to_genre();">
					<option value="">Browse genres</option>
<?php
					foreach ($genres as $g) {
						$selected = "";
						if (isset($genre)) {
							$selected = $g->slug == $genre->slug ? "selected" : "";
						}
?>
						<option value="<?php echo $g->slug; ?>" <?php echo $selected; ?>><?php echo $g->name; ?></option>
<?php
					}
?>
				</select>
			</div>
<?php
		}

		/* activity_log_record - make an activity log record */
		function activity_log_record($action, $object_id, $notes = "...")
		{
			$record = new Activity_Log();

			$record->date = date("Y-m-d H:i:s", time());
			$record->memberid = get_current_user_id();
			$record->action = $action;
			$record->object_id = $object_id;
			$record->notes = $notes;

			$this->repo->activity_log_create($record);
		}
		
		/* votes_used - get the number of votes a patron has already used */
		function votes_used($patron_id)
		{
			return $this->repo->vote_get_count_for_patron($patron_id);
		}
		
		/* votes_total - get the total number of votes a patron has */
		function votes_total($patron_id)
		{
			$dues = $this->get_membership_fee($patron_id);
			$dues_votes = min(100, $dues) / 10;

			$reviews = $this->repo->comments_get_count_by_user($patron_id);
			$reviews_votes = min(100, $reviews) / 10;

			return ceil($dues_votes) + ceil($reviews_votes);
		}

		/* votes_raw - get number of votes on a book */
		function votes_raw($item_id)
		{
			return $this->repo->vote_get_count_for_item($item_id);
		}

		/* post_id_sort - apply a sort to post fetches */
		function post_id_sort($query)
		{
			// only modify queries for 'event' post type
			if (isset($query->query_vars["post_type"]) && $query->query_vars["post_type"] == "item" && $query->is_singular()) {
				$query->set("orderby", "ID");
				$query->set("order", "ASC");
			}
			if (is_archive() && isset($query->query_vars["item_genre"])) {
				$query->set("orderby", "meta_value");
				$query->set("meta_key", "cf_last");
				$query->set("order", "ASC");
			}

			return $query;
		}

		/* search_filter - the filter for the main search page */
		function search_filter($query)
		{
			if ($query->is_search() && !empty($_GET["s"])) {
				// sanitize search query and breaks it into array of strings
				$searchfield = strtolower(trim(addslashes(str_replace('\\', '', $_GET["s"]))));
				$search_string = array_unique(preg_split('/\s+/', $searchfield));

				// prioritized search
				if ($searchfield != "") {
					$titles = array();
					$ids = array();

					// 1. exact match of title
					$books = $this->repo->post_get_by_title($searchfield);

					foreach ($books as $book) {
						if (!in_array($book->post_title, $titles)) {
							array_push($ids, $book->ID);
							array_push($titles, $book->post_title);
						}
					}

					// 2. title contains all the words in search query in order
					$books = $this->repo->post_get_by_title_contains($searchfield);

					foreach ($books as $book) {
						if (!in_array($book->post_title, $titles)) {
							array_push($ids, $book->ID);
							array_push($titles, $book->post_title);
						}
					}

					// 3. title contains all the words in the search query in any order
					$books = $this->repo->post_get_by_title_fuzzy($searchfield);

					foreach ($books as $book) {
						if (!in_array($book->post_title, $titles)) {
							array_push($ids, $book->ID);
							array_push($titles, $book->post_title);
						}
					}

					// 4. author contains all the words in search query
					$books = $this->repo->post_get_by_meta_contains("cf_author", $searchfield);

					foreach ($books as $book) {
						if (!in_array($book->post_title, $titles)) {
							array_push($ids, $book->ID);
							array_push($titles, $book->post_title);
						}
					}

					// 5. description contains all the words in search query (if we don't have enough)
					if (count($ids) < 20) {
						$books = $this->repo->post_get_by_content_contains($searchfield);

						foreach ($books as $book) {
							if (!in_array($book->post_title, $titles)) {
								array_push($ids, $book->ID);
								array_push($titles, $book->post_title);
							}
						}
					}

					// remove duplicate ids
					$ids = array_unique($ids);
				}

				$sort = "post__in";
				if (isset($_GET["sort"]) && isset($sortParams[$_GET["sort"]]))
					$sort = $sortParams[$_GET["sort"]];
				if (count($ids) == 0)
					$ids = array("");

				$args = array(
					"post__in"			=> $ids
					,"post_type"		=> "item"
					,"s"				=> ""
					,"posts_per_page"	=> 10
					,"orderby"			=> $sort
					,"paged"			=> get_query_var("paged", 1)
				);

				foreach ($args as $key=>$value) {
					$query->set($key, $value);
				}
			}

			return $query;
		}

		/* delete_item - delete an item upon declining request */
		function delete_item()
		{
			if (!isset($_POST["ID"]))
				return false;

			if (wp_delete_post($_POST["ID"], true)) {
				if (isset($_REQUEST["content"]) && $_REQUEST["content"]) {
					$request = $this->repo->vote_get_by_item($_REQUEST["ID"]);
					$subject = "Your ".$this->admin_get_shortName()." book request has been declined";
					$user = get_userdata($request->patron_id);
					$headers = "From: ".$this->admin_get_shortName()." <".$this->admin_get_email().">\r\n";
					$content = $_REQUEST["content"];

					$this->repo->vote_delete_by_item_patron($_POST["ID"], $request->patron_id);
					
					wp_mail($user->user_email, $subject, $content, $headers);
					wp_safe_redirect("/manage-requests");
				} else {
					echo "deleted";
				}
			}
		}

		/* get_last_pay - get the last time a patron has paid */
		function get_last_pay($patron_id)
		{
			$month_unix = 2592000;
			$year_unix = 31536000;

			$dues = $this->get_membership_fee($patron_id);
			$last_dues = $this->repo->transaction_get_last_by_type($patron_id, "membership");

			$user_meta = get_user_meta($patron_id);

			if (!$last_dues) {
				$months_paid = 0;
				$last_payment = 0;
			} else {
				$months_paid = $last_dues->amount / 5;
				$last_payment = $last_dues->created_at;
			}

			$dues_status = array(
				"due"		=> false
				,"danger"	=> !$last_dues || ($last_dues && time() - ($months_paid * $month_unix) > $last_payment && $dues < 100)
				,"isLifetime"	=> $dues >= 100
				,"inactive"	=> false
			);

			if ($dues_status["isLifetime"] == false) {
				if ($last_payment == 0)
					$dues_status["due"] = $user_meta["last_activity"][0];
				else
					$dues_status["due"] = ($months_paid * $month_unix) + $last_payment;

				if ($last_payment < time() - $year_unix)
					$dues_status["inactive"] = true;
			} else {
				$dues_status["due"] = $year_unix + $last_payment;

				if ($last_payment < time() - (2 * $year_unix))
					$dues_status["inactive"] = true;
			}
			return array_merge((array) $last_dues, $dues_status);
		}

		function get_membership_fee($patron_id)
		{
			return $this->repo->transaction_get_total_by_type_for_patron("Membership", $patron_id);
		}

		/* integrate_request - integrate a request */
		function integrate_request()
		{
			if (!is_numeric($_REQUEST["ID"]))
				wp_safe_redirect("/");

			$status = get_post_meta($_REQUEST["ID"], "cf_status", true);
			$post = get_post($_REQUEST["ID"]);

			if ($status == "req") {
				$subject = "Your requested item just came";
				$headers = "From: ".$this->admin_get_shortName()." <".$this->admin_get_email().">\r\n";

				$user_ids = $this->repo->vote_get_patrons_by_item($_REQUEST["ID"]);

				$this->repo->vote_delete_by_item($_REQUEST["ID"]);

				wp_update_post(array(
					"ID"				=> $_REQUEST["ID"]
					,'post_modified'	=> date("Y:m:d H:i:s")
				));

				foreach ($user_ids as $user_id)
				{
					$user = get_userdata($user_id);
					$votes = ($this->votes_total($user_id) - $this->votes_used($user_id));
					$content = $post->post_title." has just arrived and can be checked out.

		You now have $votes vote(s) you can use to vote on the request list the next time you log in at ".get_site_url()."

		".$this->admin_get_emailSig();
					wp_mail($user->user_email, $subject, $content, $headers);
				}

				update_post_meta($_REQUEST["ID"], "cf_popularity", 0);
			}
			update_post_meta($_REQUEST["ID"], "cf_status", "in");
			$this->activity_log_record("integrate", $_REQUEST["ID"], get_the_title($_REQUEST["ID"]));
			wp_safe_redirect("/item/".$post->post_name);
		}

		/* mark_as_lost - add an item to the lost list */
		function mark_as_lost()
		{
			if (!is_numeric($_REQUEST["ID"]))
				wp_safe_redirect("/");

			$status = get_post_meta($_REQUEST["ID"], "cf_status", true);
			$post = get_post($_REQUEST["ID"]);

			if ($status == "in")
				update_post_meta($_REQUEST["ID"], "cf_status", "lost");

			wp_safe_redirect("/item/".$post->post_name);
		}

		/* out_note - display the out_note form */
		function out_note()
		{
?>
			<div><h1>Add Note</h1><form id="note-form" class="form-horizontal" action="/librarian/outnote" method="post">
			<input type="hidden" name="_csrf" value="bm9RdkFMRk0UIiNCNygvBCoNEANzFSwnLzUbPHI0dXwYCxs/JQ03OA=="><input type="hidden" id="outnotes-item_id" name="OutNotes[item_id]" value="2059"><input type="hidden" id="outnotes-patron_id" name="OutNotes[patron_id]" value="11"><div id="out_title"></div><div class="row"><div class="form-group field-outnotes-comments">
			<label class="col-sm-3 control-label" for="outnotes-comments">Comments</label>
			<div class="col-sm-8"><textarea id="outnotes-comments" class="form-control" name="OutNotes[comments]" rows="6" cols="15"></textarea></div>
			<div class="col-sm-8"><div class="help-block"></div></div>
			</div></div><div class="row"><div class="form-group field-outnotes-color required">
			<label class="col-sm-3 control-label" for="outnotes-color">Color</label>
			<div class="col-sm-8"><input type="hidden" name="OutNotes[color]" value=""><div id="outnotes-color"><label class="radio-inline"><input type="radio" name="OutNotes[color]" value="green"><font color="green">green</font></label>
			<label class="radio-inline"><input type="radio" name="OutNotes[color]" value="red"><font color="red">red</font></label>
			<label class="radio-inline"><input type="radio" name="OutNotes[color]" value="blue"><font color="blue">blue</font></label></div></div>
			<div class="col-sm-8"><div class="help-block"></div></div>
			</div></div><button type="submit">Create</button></form></div>
<?php
		}

		/* patrons */
		function patrons_get_active()
		{
			return json_encode(get_users(array(
				"fields" => array(
					"ID"
					,"display_name"
				)
				,"orderby" => "display_name"
			)));
		}

		/* transactions */

		/* transactions_get - prepare view-model for transaction page */
		function transactions_get()
		{
			$user_meta = get_userdata(get_current_user_id());

			$viewModel = new TransactionViewModel();
			$viewModel->can_manage_circulation = current_user_can("manage_circulation");

			$viewModel->transaction_methods = $this->transactions_get_methods();
			$viewModel->transaction_types = $this->transactions_get_types();
			$viewModel->transactions = $this->transactions_get_all();
			$viewModel->patrons = $this->patrons_get_active();
			$viewModel->profile_link = $this->get_profile_link_base();

			if ($user_meta)
				$viewModel->is_admin = in_array("administrator", $user_meta->roles);
			else
				$viewModel->is_admin = false;
			
			$viewModel->incoming_chart_id = "incoming-chart";
			$viewModel->outgoing_chart_id = "outgoing-chart";
			$viewModel->table_id = "transactions-table";
			$viewModel->modal_id = "transactions-modal-form";

			return $viewModel;
		}

		/* transactions_get_all - get a list of transactions */
		function transactions_get_all()
		{
			return json_encode($this->repo->transaction_get_all());
		}

		/* transactions_get_methods - get list of transaction methods */
		function transactions_get_methods()
		{
			return json_encode($this->repo->transaction_method_get_all());
		}

		/* transactions_get_types - get list of transaction types */
		function transactions_get_types()
		{
			return json_encode($this->repo->transaction_type_get_all());
		}

		/* transactions_ajax_by_month_by_type - get transactions by month and type */
		function transactions_ajax_by_month_by_type()
		{
			$_GET["month"];
			$_GET["type_id"];
			echo $_GET["month"].$_GET["type_id"];
			wp_die();
		}

		function transactions_ajax_get()
		{
			echo json_encode($this->repo->transaction_get_all());
			wp_die();
		}

		/* delete_transaction - remove a transaction from the table */
		function delete_transaction()
		{
			if (is_admin() && isset($_REQUEST["transaction_id"]))
				$this->repo->transaction_delete($_REQUEST["transaction_id"]);

			$this->chart_reload();
		}

		function get_profile_link_base()
		{
			// I feel like this isn't always true...
			return "/profile/";
		}
	}

	global $AltLibrarian;
	$AltLibrarian = new AltLibrarian();
}
