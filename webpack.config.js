const path = require("path");
const TerserPlugin = require("terser-webpack-plugin");
const CopyPlugin = require("copy-webpack-plugin");

const entry = {
	front: [
		"./sass/front.scss"
		,"./typescript/front.ts"
	]
};

const export_module = {
	rules: [{
		test: /\.ts$/
		,use: "ts-loader"
	}
	,{
		test: /\.s[ac]ss$/i
		,use: [
			"style-loader"
			,"css-loader"
			,"sass-loader"
		]
	}]
};

const resolve = {
	extensions: [".ts", ".js", ".scss"]
};

const output_filename = "js/[name].min.js";

const output_library = "altlib";

const optimization = {
	minimize: true
	,minimizer: [new TerserPlugin({
		terserOptions: {
			mangle: false
		}
	})]
};

const plugins = [
	new CopyPlugin({
		patterns: [
			{	from: "AltLibrarian.php"
				,to: ""
			},{	from: "AltLibrarian_Repo.php"
				,to: ""
			},{	from: "COPYING"
				,to: ""
			},{	from: "templates"
				,to: "templates"
			},{	from: "node_modules/@popperjs/core/dist/umd/popper.min.js"
				,to: "js/popper.min.js"
			},{	from: "node_modules/bootstrap/dist/js/bootstrap.min.js"
				,to: "js/bootstrap.min.js"
			},{	from: "node_modules/corejs-typeahead/dist/typeahead.bundle.min.js"
				,to: "js/typeahead.bundle.min.js"
			},{	from: "node_modules/jquery-ui-dist/jquery-ui.min.js"
				,to: "js/jquery-ui.min.js"
			},{	from: "node_modules/open-iconic/font/fonts"
				,to: "fonts"
			},{	from: "node_modules/bootstrap/dist/css/bootstrap.min.css"
				,to: "css/bootstrap.min.css"
			},{	from: "node_modules/open-iconic/font/css/open-iconic.min.css"
				,to: "css/open-iconic.min.css"
			},{	from: "node_modules/open-iconic/font/css/open-iconic-bootstrap.min.css"
				,to: "css/open-iconic-bootstrap.min.css"
			},{	from: "node_modules/jquery-ui-dist/jquery-ui.min.css"
				,to: "css/jquery-ui.min.css"
			},{	from: "node_modules/jquery-ui-dist/images"
				,to: "css/images"
			}
		]
	})
];

const dist = {
	entry: entry
	,name: "dist"
	,mode: "production"
	,module: export_module
	,resolve: resolve
	,output: {
		filename: output_filename
		,path: path.resolve(__dirname, "dist")
		,library: output_library
	}
	,optimization: optimization
	,plugins: plugins
};

const dev = {
	entry: entry
	,name: "dev"
	,mode: "development"
	,devtool: "source-map"
	,devServer: {
		static: ["dev"],
		devMiddleware: { writeToDisk: true },
		proxy: { "/": { target: "http://localhost:80/" } },
		open: true
	}
	,module: export_module
	,resolve: resolve
	,output: {
		filename: output_filename
		,path: path.resolve(__dirname, "dev")
		,library: output_library
	}
	,optimization: optimization
	,plugins: plugins
};

module.exports = [ dist, dev ];
