<?php

if (!class_exists("AltLibrarian_Repo")) {
    class Post {
        public $ID;
        public $post_author;
        public $post_date;
        public $post_date_gmt;
        public $post_content;
        public $post_title;
        public $post_excerpt;
        public $post_status;
        public $comment_status;
        public $ping_status;
        public $post_password;
        public $post_name;
        public $to_ping;
        public $pinged;
        public $post_modified;
        public $post_modified_gmt;
        public $post_content_filtered;
        public $post_parent;
        public $guid;
        public $menu_order;
        public $post_type;
        public $post_mime_type;
        public $comment_count;

        public $PostMetas;

        function __construct()
        {
            $PostMetas = array();
        }
    }

    class PostMeta {
        public $meta_id;
        public $post_id;
        public $meta_key;
        public $meta_value;

        function __construct()
        {}
    }

    class Activity_Log {
        public $id;
        public $date;
        public $memberid;
        public $action;
        public $object_id;
        public $notes;

        function __construct()
        {}
    }

    class Out_Note {
        public $id;
        public $item_id;
        public $patron_id;
        public $comments;
        public $color;
        public $created_at;

        function __construct()
        {}
    }

    class Record_Status {
        public $id;
        public $description;
        public $created_at;
        public $created_by;
        public $modified_at;
        public $modified_by;

        function __construct()
        {}
    }

    class Transaction {
        public $id;
        public $description;
        public $type;
        public $method;
        public $amount;
        public $patron_id;
        public $created_at;
        public $created_by;
        public $record_status;

        function __construct()
        {}
    }

    class Transaction_Method {
        public $id;
        public $description;
        public $created_at;
        public $created_by;
        public $modified_at;
        public $modified_by;
        public $record_status;

        function __construct()
        {}
    }

    class Transaction_Type {
        public $id;
        public $description;
        public $is_payout;
        public $patron_required;
        public $reason_required;
        public $minimum_amount;
        public $created_at;
        public $created_by;
        public $modified_at;
        public $modified_by;
        public $record_status;

        function __construct()
        {}
    }

    class Vote {
        public $timestamp;
        public $created;
        public $patron_id;
        public $item_id;
        public $status;

        function __construct()
        {}
    }

    class AltLibrarian_Repo {
        private $wpdb;

        function __construct()
        {
            global $wpdb;
            $this->wpdb = $wpdb;
        }

        function db_create()
        {
            require_once(ABSPATH."wp-admin/includes/upgrade.php");
            
            $activity_log = "CREATE TABLE activity_log (
				id 				INT			NOT NULL	AUTO_INCREMENT
				,date 			DATETIME	NOT NULL
				,memberid 		INT			NOT NULL
				,action			TEXT
				,object_id		INT						DEFAULT NULL
				,notes			TEXT
				,PRIMARY KEY  (id)
			);";

			$out_notes = "CREATE TABLE out_notes (
				id				INT			NOT NULL	AUTO_INCREMENT
				,item_id		INT						DEFAULT NULL
				,patron_id		INT						DEFAULT NULL
				,comments		TEXT
				,color			VARCHAR(45)				DEFAULT NULL
				,created_at		INT			NOT NULL
				,PRIMARY KEY  (id)
			);";

            $record_status = "CREATE TABLE record_status (
                id              INT         NOT NULL    AUTO_INCREMENT
                ,description    TINYTEXT    NOT NULL
                ,created_at     INT         NOT NULL
                ,created_by     INT         NOT NULL
                ,modified_at    INT         NOT NULL
                ,modified_by    INT         NOT NULL
                ,PRIMARY KEY  (id)
            );";

			$transaction = "CREATE TABLE transaction (
                id				INT			NOT NULL	AUTO_INCREMENT
                ,description	TEXT
                ,type			INT         NOT NULL
                ,method			INT			NOT NULL       
                ,amount			DOUBLE		NOT NULL	DEFAULT '0'
                ,patron_id		INT
                ,created_at		INT			NOT NULL
                ,created_by     INT         NOT NULL
                ,record_status  TINYINT     NOT NULL    DEFAULT '1'
                ,PRIMARY KEY  (id)
            );";

            $transaction_methods = "CREATE TABLE transaction_method (
                id              INT         NOT NULL    AUTO_INCREMENT
                ,description    TINYTEXT    NOT NULL
                ,created_at     INT         NOT NULL
                ,created_by     INT         NOT NULL
                ,modified_at    INT         NOT NULL
                ,modified_by    INT         NOT NULL
                ,record_status  TINYINT     NOT NULL    DEFAULT '1'
                ,PRIMARY KEY  (id)
            );";

            $transaction_types = "CREATE TABLE transaction_type (
                id                  INT         NOT NULL    AUTO_INCREMENT
                ,description        TINYTEXT    NOT NULL
                ,is_payout          BOOLEAN     NOT NULL
                ,patron_required    BOOLEAN     NOT NULL    DEFAULT '0'
                ,reason_required    BOOLEAN     NOT NULL    DEFAULT '1'
                ,minimum_amount     DOUBLE      NOT NULL    DEFAULT '0'
                ,created_at         INT         NOT NULL
                ,created_by         INT         NOT NULL
                ,modified_at        INT         NOT NULL
                ,modified_by        INT         NOT NULL
                ,record_status      TINYINT     NOT NULL    DEFAULT '1'
                ,PRIMARY KEY  (id)
            );";

			$votes = "CREATE TABLE votes (
				timestamp		INT			NOT NULL	AUTO_INCREMENT
				,created		DATETIME	NOT NULL
				,patron_id		INT			NOT NULL
				,item_id		INT			NOT NULL
				,status			TINYINT					DEFAULT '0'
				,PRIMARY KEY  (timestamp)
			);";

			dbDelta($activity_log);
			dbDelta($out_notes);
            dbDelta($record_status);
			dbDelta($transaction);
            dbDelta($transaction_methods);
            dbDelta($transaction_types);
			dbDelta($votes);
        }

        function post_get_by_id($ID)
        {
            $result = $this->wpdb->get_row($this->wpdb->prepare(
                "SELECT *
                FROM ".$this->wpdb->posts."
                WHERE
                    ID = %d"
                ,$ID
            ));

            return $result;
        }

        function post_get_by_title($post_title)
        {
            $result = $this->wpdb->get_results($this->wpdb->prepare(
                "SELECT *
                FROM ".$this->wpdb->posts."
                WHERE
                    post_title = '%s'
                ORDER BY post_date DESC"
                ,$post_title
            ));

            return $result;  
        }

        function post_get_by_title_prefix($prefix)
        {
            $prefix .= "%";

            $result = $this->wpdb->get_results($this->wpdb->prepare(
                "SELECT *
                FROM ".$this->wpdb->posts."
                WHERE
                    post_title LIKE '%s'
                ORDER BY
                    post_title"
                ,$prefix
            ));

            return $result;
        }

        function post_get_by_title_contains($query_string)
        {
            $query_string = "%".$query_string."%";

            $result = $this->wpdb->get_results($this->wpdb->prepare(
                "SELECT *
                FROM ".$this->wpdb->posts."
                WHERE
                    post_title LIKE '%s'
                ORDER BY
                    post_title"
                ,$query_string
            ));

            return $result;
        }

        function post_get_by_title_fuzzy($query_string)
        {
            $query = "SELECT * FROM ".$this->wpdb->posts." WHERE ";

            $query_vals = explode(" ", $query_string);

            foreach ($query_vals as $query_val) {
                $query_val_fuzzy = "%".$query_val."%";
                $query .= "post_title LIKE '".$query_val_fuzzy."' AND ";
            }

            $query .= "post_title IS NOT NULL";

            $result = $this->wpdb->get_results($query);

            return $result;
        }

        function post_get_by_content_contains($query_string)
        {
            $query_string = "%".$query_string."%";

            $result = $this->wpdb->get_results($this->wpdb->prepare(
                "SELECT *
                FROM ".$this->wpdb->posts."
                WHERE
                    post_content LIKE '%s'
                ORDER BY
                    post_title"
                ,$query_string
            ));

            return $result;          
        }

        function post_get_by_name($post_name)
        {
            $result = $this->wpdb->get_results($this->wpdb->prepare(
                "SELECT *
                FROM ".$this->wpdb->posts."
                WHERE
                    post_name = '%s'
                ORDER BY post_date DESC"
                ,$post_name
            ));

            return $result;
        }

        function post_get_by_meta_with_meta($meta_key, $meta_value)
        {
            $results = array();

            $query = new WP_Query(array(
                "post_type"     => "item"
                ,"nopaging"     => true
                ,"meta_query"   => array(array(
                    "key"           => $meta_key
                    ,"value"        => $meta_value
                    ,"compare"      => "="
            ))));

            while ($query->have_posts()) {
                $query->the_post();
                $post = new Post();

                $post->ID = get_the_ID();
                $post->post_title = get_the_title();

                $post->PostMetas["cf_author"] = get_post_meta($post->ID, "cf_author", true);
                $post->PostMetas["cf_outdate"] = get_post_meta($post->ID, "cf_outdate", true);

                array_push($results, $post);
            }

            return $results;
        }

        function post_get_by_meta_contains($meta_key, $meta_value)
        {
            $meta_value = "%".$meta_value."%";

            $result = $this->wpdb->get_results($this->wpdb->prepare(
                "SELECT p.*
                FROM ".$this->wpdb->posts." p
                INNER JOIN ".$this->wpdb->postmeta." pm
                ON  (
                    p.ID = pm.post_id
                )
                WHERE
                    pm.meta_key = '%s'
                    AND pm.meta_value LIKE '%s'
                ORDER BY post_date DESC"
                ,$meta_key
                ,$meta_value
            ));

            return $result;
        }

        function post_get_by_meta($meta_key, $meta_value)
        {
            $result = $this->wpdb->get_results($this->wpdb->prepare(
                "SELECT p.*
                FROM ".$this->wpdb->posts." p
                INNER JOIN ".$this->wpdb->postmeta." pm
                ON  (
                    p.ID = pm.post_id
                )
                WHERE
                    pm.meta_key = '%s'
                    AND pm.meta_value = '%s'
                ORDER BY post_title"
                ,$meta_key
                ,$meta_value
            ));

            return $result;
        }

        function post_get_for_search($post_ids, $status)
        {
            $loaded_posts = array();
            $result = array();
            
            $query = new WP_Query(array(
                "post__in"      => $post_ids
                ,"post_type"    => "item"
                ,"orderby"      => "post__in"
                ,"meta_query"   => array(array(
                    "key"           => "cf_status"
                    ,"value"        => $status
                    ,"compare"      => "="
                ))));

            while ($query->have_posts()) {
                $query->the_post();
                $post = new Post();

                $post->post_title = get_the_title();

                if (in_array($post->post_title, $loaded_posts))
                    continue;

                $post->ID = get_the_ID();

                array_push($loaded_posts, $post->post_title);
                array_push($result, $post);
            }

            return $result;
        }

        function post_get_ids_by_title($post_title)
        {
            $result = $this->wpdb->get_col($this->wpdb->prepare(
                "SELECT ID
                FROM ".$this->wpdb->posts."
                WHERE
                    post_title = '%s'"
                ,$post_title
            ));

            return $result;
        }

        function post_get_ids_by_parent($parent_id)
        {
            $result = $this->wpdb->get_col($this->wpdb->prepare(
                "SELECT ID
                FROM ".$this->wpdb->posts."
                WHERE post_parent = %d"
                ,$parent_id
            ));

            return $result;
        }

        function post_get_next_id()
        {
            $result = $this->wpdb->get_var($this->wpdb->prepare(
                "SELECT MAX(ID) + 1
                FROM ".$this->wpdb->posts
            ));

            return $result;
        }

        function post_create($post)
        {
            $next_id = $this->post_get_next_id();
            $posting_date = date("Y-m-d H:i:s");
            $posting_date_gmt = gmdate("Y-m-d H:i:s");

            $this->wpdb->insert(
                $this->wpdb->posts
                ,array(
                    "ID"					=> $next_id
                    ,"post_content"			=> $post->post_content
                    ,"post_title"			=> $post->post_title
                    ,"post_status"			=> "publish"
                    ,"post_parent"			=> $post->post_parent
                    ,"post_name"			=> sanitize_title($post->post_title)
                    ,"post_type"			=> "item"
                    ,"guid"					=> $next_id
                    ,"post_date"			=> $posting_date
                    ,"post_date_gmt"		=> $posting_date_gmt
                    ,"post_modified"		=> $posting_date
                    ,"post_modified_gmt"	=> $posting_date_gmt
                    ,"comment_status"		=> "open"
                    ,"ping_status"			=> "open"
            ));

            return $next_id;
        }

        function post_update($post)
        {
            $this->wpdb->query($this->wpdb->prepare(
                "UPDATE ".$this->wpdb->posts."
                SET
                    post_content = '%s'
                    ,post_title = '%s'
                    ,post_name = '%s'
                WHERE
                    ID = %d"
                ,$post->post_content
                ,$post->post_title
                ,sanitize_title($post->post_title)
                ,$post->ID
            ));

            return $post->ID;
        }

        function users_get_active()
        {
            $results = $this->wpdb->get_results(
                "SELECT *
                FROM ".$this->wpdb->users
            );

            return $results;
        }

        function user_get_patron_list()
        {
            $results = $this->wpdb->get_results(
                "SELECT
                    wpu.ID              AS ID
                    ,wpu.display_name   AS Name
                    ,wpu.user_email     AS Email
                    ,wpum.meta_value    AS LastActivity
                FROM ".$this->wpdb->users." wpu
                JOIN ".$this->wpdb->usermeta." wpum
                ON  (
                    wpum.user_id = wpu.ID
                )
                WHERE
                    wpum.meta_key = 'last_activity'
                ORDER BY
                    wpum.meta_value DESC;"
            );

            return $results;
        }

        function user_set_active($user_id)
        {
            $this->wpdb->query($this->wpdb->prepare(
				"UPDATE ".$this->wpdb->users."
				SET
                    user_status = 0
                WHERE
                    ID = %d"
				,$user_id));
        }

        function comments_get_subset($index, $count)
        {
            $result = $this->wpdb->get_results($this->wpdb->prepare(
                "SELECT *
                FROM ".$this->wpdb->comments."
                ORDER BY comment_date DESC
                LIMIT %d, %d"
                ,$index
                ,$count
            ));

            return $result;
        }

        function comments_get_count()
        {
            $result = $this->wpdb->get_var(
                "SELECT COUNT(comment_ID)
                FROM ".$this->wpdb->comments
            );

            return $result;
        }

        function comments_get_count_by_user($user_id)
        {
            $result = $this->wpdb->get_var($this->wpdb->prepare(
                "SELECT COUNT(comment_ID)
                FROM ".$this->wpdb->comments."
                WHERE user_id = %d"
                ,$user_id
            ));

            return $result;
        }

        function activity_log_create($record)
        {
            $this->wpdb->query($this->wpdb->prepare(
                "INSERT INTO activity_log (date, memberid, action, object_id, notes)
                VALUES (%s, %d, %s, %d, %s)"
                ,$record->date
                ,$record->memberid
                ,$record->action
                ,$record->object_id
                ,$record->notes
            ));
        }

        function out_note_get_by_item($item_id)
        {
            $result = $this->wpdb->get_results($this->wpdb->prepare(
                "SELECT *
                FROM out_notes
                WHERE
                    item_id = %d
                ORDER BY id DESC
                LIMIT 5"
                ,$item_id
            ));

            return $result;
        }

        function out_note_create($out_note)
        {
            $this->wpdb->query($this->wpdb->prepare(
                "INSERT INTO out_notes (item_id, patron_id, comments, color, created_at)
                VALUES (%d, %d, %s, %s, %d)"
                ,$out_note->item_id
                ,$out_note->patron_id
                ,$out_note->comments
                ,$out_note->color
                ,$out_note->created_at
            ));
        }

        function out_note_delete($id)
        {
            $this->wpdb->query($this->wpdb->prepare(
                "DELETE
                FROM out_notes
                WHERE
                    id = %d"
                ,$id
            ));
        }

        function out_note_delete_by_item($item_id)
        {
            $this->wpdb->query($this->wpdb->prepare(
                "DELETE
                FROM out_notes
                WHERE
                    item_id = %d"
                ,$item_id
            ));
        }

        function status_get_all()
        {
            return $this->wpdb->query(
                "SELECT *
                FROM record_status"
            );
        }

        function transaction_get_all()
        {
            return $this->wpdb->get_results(
                "SELECT *
                FROM transaction
                ORDER BY
                    created_at DESC"
            );
        }

        function transaction_get_last_by_type($patron_id, $type)
        {
            $result = $this->wpdb->get_row($this->wpdb->prepare(
                "SELECT t.*
                FROM transaction t
                INNER JOIN transaction_type tt
                ON  (
                    t.type = tt.id
                )
                WHERE
                    t.patron_id = %d
                    AND tt.description = %s
                ORDER BY id DESC
                LIMIT 1"
                ,$patron_id
                ,$type
            ));

            return $result;
        }

        function transaction_get_for_timespan($start_time, $end_time)
        {
            $result = $this->wpdb->get_results($this->wpdb->prepare(
                "SELECT *
                FROM transaction
                WHERE
                    created_at >= %d
                    AND created_at < %d
                ORDER BY created_at DESC"
                ,$start_time
                ,$end_time
            ));

            return $result;
        }

        function transaction_get_total_by_type_for_patron($type, $patron_id)
        {
            $result = $this->wpdb->get_var($this->wpdb->prepare(
               "SELECT SUM(t.amount)
                FROM transaction t
                INNER JOIN transaction_type tt
                ON (
                    t.type = tt.id
                )
                WHERE
                   tt.description = %s
                   AND t.patron_id = %d"
                ,$type
                ,$patron_id
            ));

            return $result;
        }

        function transaction_get_total_by_type_for_timespan($type, $start_time, $end_time)
        {
            $result = $this->wpdb->get_var($this->wpdb->prepare(
                "SELECT SUM(t.amount)
                FROM transaction t
                INNER JOIN transaction_type tt
                ON  (
                    t.type = tt.id
                )
                WHERE
                    tt.description = %s
                    AND t.created_at >= %d
                    AND t.created_at < %d
                ORDER BY t.created_at DESC"
                ,$type
                ,$start_time
                ,$end_time
            ));

            return $result;
        }

        function transaction_create($transaction)
        {
            $this->wpdb->query($this->wpdb->prepare(
                "INSERT INTO transaction (patron_id, amount, description, type, method, created_at, created_by)
                VALUES (%d, %d, %s, %d, %d, %d, %d)"
                ,$transaction->patron_id
                ,$transaction->amount
                ,$transaction->description
                ,$transaction->type
                ,$transaction->method
                ,$transaction->created_at
                ,$transaction->created_by
            ));
        }

        function transaction_delete($id)
        {
            $this->wpdb->query($this->wpdb->prepare(
                "DELETE
                FROM transaction
                WHERE id = %d"
                ,$id
            ));
        }

        function transaction_method_get_all()
        {
            return $this->wpdb->get_results(
                "SELECT *
                FROM transaction_method"
            );
        }

        function transaction_method_get_id_by_desc($method_desc)
        {
            return $this->wpdb->get_var($this->wpdb->prepare(
                "SELECT id
                FROM transaction_method
                WHERE
                    description = '%s'"
                ,$method_desc
            ));
        }

        function transaction_type_get_all()
        {
            return $this->wpdb->get_results(
                "SELECT *
                FROM transaction_type"
            );
        }

        function transaction_type_get_id_by_desc($type_desc)
        {
            return $this->wpdb->get_var($this->wpdb->prepare(
                "SELECT id
                FROM transaction_type
                WHERE
                    description = '%s'"
                ,$type_desc
            ));
        }

        function vote_get_by_item($item_id)
        {
            $result = $this->wpdb->get_row($this->wpdb->prepare(
                "SELECT *
                FROM votes
                WHERE
                    item_id = %d"
                ,$item_id
            ));

            return $result;
        }

        function vote_get_count_for_item($item_id)
        {
            $result = $this->wpdb->get_var($this->wpdb->prepare(
                "SELECT COUNT(timestamp)
                FROM votes
                WHERE
                    status = 0
                    AND item_id = %d"
                ,$item_id
            ));

            return $result;
        }

        function vote_get_count_for_patron($patron_id)
        {
            $result = $this->wpdb->get_var($this->wpdb->prepare(
                "SELECT COUNT(timestamp)
                FROM votes
                WHERE
                    status = 0
                    AND patron_id = %d"
                ,$patron_id
            ));

            return $result;
        }

        function vote_get_count_for_item_patron($item_id, $patron_id)
        {
            $result = $this->wpdb->get_var($this->wpdb->prepare(
                "SELECT COUNT(timestamp)
                FROM votes
                WHERE
                    status = 0
                    AND item_id = %d
                    AND patron_id = %d"
                ,$item_id
                ,$patron_id
            ));

            return $result; 
        }

        function vote_get_ids_by_patron($patron_id)
        {
            $result = $this->wpdb->get_col($this->wpdb->prepare(
                "SELECT item_id
                FROM votes
                WHERE
                    status = 0
                    AND patron_id = %d"
                ,$patron_id
            ));

            return $result;
        }

        function vote_get_patrons_by_item($item_id)
        {
            $result = $this->wpdb->get_col($this->wpdb->prepare(
                "SELECT patron_id
                FROM votes
                WHERE
                    item_id = %d"
                ,$item_id
            ));

            return $result;
        }

        function vote_create($vote)
        {
            $this->wpdb->query($this->wpdb->prepare(
                "INSERT INTO votes
                VALUES (%d, %d, %d, %d, %d)"
                ,$vote->timestamp
                ,$vote->created
                ,$vote->patron_id
                ,$vote->item_id
                ,$vote->status
            ));
        }

        function vote_delete_by_item($item_id)
        {
            $this->wpdb->query($this->wpdb->prepare(
                "DELETE
                FROM votes
                WHERE
                    item_id = %d"
                ,$item_id
            ));
        }

        function vote_delete_by_item_patron($item_id, $patron_id)
        {
            $this->wpdb->query($this->wpdb->prepare(
                "DELETE
                FROM votes
                WHERE
                    item_id = %d
                    AND patron_id = %d
                ORDER BY timestamp DESC
                LIMIT 1"
                ,$item_id
                ,$patron_id
            ));
        }
    }
}