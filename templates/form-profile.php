<?php
$meta_values = array(
    "nickname"		=> ""
    ,"description"	=> "");

$user_info = get_userdata(get_current_user_id());

if ($user_info) {
    foreach ($meta_values as $key => &$value) {
        $value = get_user_meta($user_info->ID, $key, true);
    }
}

get_header();
?>
		<!-- <main> -->
				<div class="container mt-3">
					<div class="row">
						<div class="col-12 col-lg-9">
							<form method="post" action="/wp-admin/admin-post.php">
								<input name="action" type="hidden" value="edit_profile">
								<div class="col-12 col-lg-10 offset-lg-2">
									<h3>Edit Profile</h3>
								</div>
								<div class="form-group col-12 row">
									<label class="col-12 col-lg-2 col-form-label text-lg-right" for="patron-name">Nickname</label>
									<div class="col-12 col-lg-10">
										<input type="text" id="patron-name" class="form-control form-control-sm" name="nickname" value="<?php echo $meta_values["nickname"]; ?>">
									</div>
								</div>

								<div class="form-group col-12 row">
									<label class="col-12 col-lg-2 col-form-label text-lg-right" for="patron-notes">Bio</label>
									<div class="col-12 col-lg-10">
										<textarea class="form-control form-control-sm" name="notes"><?php echo $meta_values["description"]; ?></textarea>
									</div>
								</div>
								<div class="col-12 col-lg-10 offset-lg-2">
									<button type="submit" class="btn btn-primary btn-sm">Save</button>
								</div>
							</form>
						</div>
					</div>
				</div>
		<!-- </main> -->
<?php get_footer();
