<?php
global $AltLibrarian;

$signature = $AltLibrarian->admin_get_emailSig();
$name = $AltLibrarian->admin_get_shortName();

$wp_query = new WP_Query(array(
	"posts_per_page"	=> "10"
    ,"post_type"		=> "item"
    ,"orderby"			=> "meta_value_num"
    ,"order"			=> "ASC"
    ,"paged"			=> $paged
    ,"meta_key"			=> "cf_outdate"
    ,"meta_query"		=> array(array(
		"key"		=> "cf_status",
		"value"		=> array("lost", "in", "req"),
		"compare"	=> "NOT IN"))));

get_header();
?>
	<!-- <main> -->
			<div class="container mt-3">
				<div class="row">
<?php
					if (current_user_can("manage_circulation")) {
?>
						<div id="content" class="col-12 col-lg-9" role="main">
							<h3>Out List</h3>
<?php
							echo paginate_links(array(
								"base"		=> str_replace(9999999, "%#%", esc_url(get_pagenum_link(9999999)))
								,"format"	=> "?paged=%#%"
								,"current"	=> max(1, get_query_var("paged"))
								,"total"	=> $wp_query->max_num_pages));
?>
							<div class="table-responsive">
								<table class="table table-striped table-sm">
<?php
									if (have_posts()) {
										while (have_posts()) {
											the_post();
											$item_status = get_post_meta(get_the_ID(), "cf_status", true);
											$out_date = get_post_meta(get_the_ID(), "cf_outdate", true);
											$userinfo = get_userdata($item_status);
											$mail = json_encode([
												"id"			=> $userinfo->ID
												,"display_name"	=> $userinfo->display_name
												,"user_email"	=> $userinfo->user_email]);
?>
										<tr>
											<td><a href="<?php the_permalink(); ?>"><?php echo the_title(); ?></a></td>
											<td>
												<?php echo "<a href='/profile/".$item_status."'>".$userinfo->display_name."</a>"; ?><br>
												<?php echo $userinfo->phone; ?>
											</td>
											<td><?php echo human_time_diff($out_date); ?> ago</td>
											<td>
												<div id="notes-<?php echo get_the_ID(); ?>">
													<?php $AltLibrarian->get_out_notes(get_the_ID()); ?>
												</div>
												<a class="btn btn-sm btn-primary" onclick='altlib.note.create(<?php echo get_the_ID(); ?>, <?php echo $item_status; ?>);'>Add Note</a>
											</td>
											<td>
												<a class="btn btn-sm btn-primary" onclick='sendEmail(<?php echo $mail; ?>)'> Send Email</a>
											</td>
										</tr>
<?php
										}
									}
?>
								</table>
							</div>
<?php
							echo paginate_links(array(
								"base"		=> str_replace(9999999, "%#%", esc_url(get_pagenum_link(9999999)))
								,"format"	=> "?paged=%#%"
								,"current"	=> max(1, get_query_var("paged"))
								,"total"	=> $wp_query->max_num_pages));
?>
						</div>
<?php
					} else {
?>
						You're not supposed to be here...
<?php
					}
?>
				</div>
			</div>
				<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="myModalLabel">Add Note</h4>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<form id="note-form" class="form-horizontal">
									<input type="hidden" name="action" value="save_note">
									<input type="hidden" id="outnotes-item_id" name="OutNotes[item_id]" value="">
									<input type="hidden" id="outnotes-patron_id" name="OutNotes[patron_id]" value="">
									<div id="out_title"></div>
									<div class="row">
										<div class="col-12 form-group field-outnotes-comments">
											<label class="col-12 control-label" for="outnotes-comments">Comments</label>
											<div class="col-12">
												<textarea id="outnotes-comments" class="form-control form-control-sm" name="OutNotes[comments]" rows="6" cols="15"></textarea>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-12 form-group field-outnotes-color required">
											<label class="col-12 control-label" for="outnotes-color">Color</label>
											<div class="col-12">
												<div id="outnotes-color">
													<label class="radio-inline"><input type="radio" name="OutNotes[color]" value="green"><font color="green">green</font></label> <label class="radio-inline"><input type="radio" name="OutNotes[color]" value="red" checked><font color="red">red</font></label> <label class="radio-inline"><input type="radio" name="OutNotes[color]" value="blue"><font color="blue">blue</font></label>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
								<button type="button" class="btn btn-primary btn-sm" onclick="altlib.note.save();">Create Note</button>
							</div>
						</div>
					</div>
				</div>
			<form id="vote-form" class="d-none" method="post" action="/wp-admin/admin-post.php">
				<input id="ID" name="ID" type="hidden">
				<input id="action" name="action" type="hidden">
			</form>
	<!-- </main> -->
<script type="text/javascript">
if (typeof $ == "undefined")
    $ = jQuery;

function sendEmail(user)
{
    $.ajax({
		url:		'/wp-admin/admin-ajax.php'
		,method:	'POST'
		,async:		false
		,dataType:	'json'
		,data: {
			action:		'outlist'
			,patron_id:	user.id
		}
		,success: function(results)
		{
			var tempVars = {
				membership_due:	results.membership_fee.danger ? 'Additionally, your membership payment is due, and if you\'d like to check something out the next time you come in, please remember to bring $5 to renew your membership :)' : ''
				,overdue: {
					count:	0
					,data:	''
				}
				,very_overdue: {
					count:	0
					,data:	''
				}
			};
			results.books.forEach(function(item) {
				var oneMthAgo = Math.floor((Date.now() - ((30 * 24 * 60 * 60) * 1000)) / 1000);
				var twoYrsAgo = Math.floor((Date.now() - ((365.25 * 24 * 60 * 60 * 2) * 1000)) / 1000);
				var outdateInt = parseInt(item.outdate);
				
				if (outdateInt >= oneMthAgo)
					return;

				var tempName = item.outdate >= twoYrsAgo ? 'overdue' : 'very_overdue';
				tempVars[tempName].data += `\t * ${item.title} - ${item.author}\n`;
				tempVars[tempName].count += 1;
				var form = {
					'action': 				'save_note'
					,'OutNotes[item_id]':	item.id
					,'OutNotes[patron_id]':	user.id
					,'OutNotes[color]':		'blue'
					,'OutNotes[comments]':	'auto-generated overdue email created by <?php $user = wp_get_current_user(); echo $user->data->display_name; ?>'
				}
				$.post(
					"/wp-admin/admin-post.php"
					,form
					,function(data)
					{
						$("#notes-" + item.id).html(data);
					});
			});

			var fname = user.display_name.split(" ")[0];
			var template = {
				signature:	`<?php echo $signature; ?>`
				,overdue: {
					single: {
						subj:	'<?php echo $name; ?>' + ' - Overdue Book'
						,body:	`Hey ${fname || user.display_name},

Hopefully you've enjoyed the book, but we'd kindly like to remind you that the following item is overdue:

${tempVars.overdue.data}
If you are unable to return it sometime this week for any reason, let us know. If you'd like to renew the book, please reply to this email or ask us when you come in. ${tempVars.membership_due}

Thank you for your support,
Your Friendly Neighborhood Librarians`}
					,plural: {
						subj: '<?php echo $name; ?>' + ' - Overdue Books'
						,body: `Hey ${fname || user.display_name},

Hopefully you've enjoyed them, but we'd kindly like to remind you that the following items are overdue:

${tempVars.overdue.data}
If you are unable to return them sometime this week for any reason, let us know. If you'd like to renew them, please reply to this email or ask us when you come in. ${tempVars.membership_due}

Thank you for your support,
Your Friendly Neighborhood Librarians`}}
				,very_overdue: {
					single: {
						subj: '<?php echo $name; ?>' + ' - Very Overdue Book'
						,body: `Hey ${fname || user.display_name},

We have a book on record still, checked out to you from a few years ago.

Title - Author - Replacement Cost
${tempVars.very_overdue.data}
Is it lost or do you still have it? We would really appreciate if you could help us replace it. If it's lost and you can't make it to the library, you can send us a payment via PayPal (use this email address) to reimburse us for the cost of replacement. Our library has a lot of funky books that can be hard to access otherwise and we are supported and run by our members.

Thank you for your support,
Your Friendly Neighborhood Librarians`}
					,plural: {
						subj: '<?php echo $name; ?>' + ' - Very Overdue Books'
						,body: `Hey ${fname || user.display_name},

We have some books on record checked out to you, which you checked out a few years ago.

Title - Author - Replacement Cost
${tempVars.very_overdue.data}
Total Replacement Cost: $

Are they lost or do you still have them? We would really appreciate if you could help us replace them. If they're lost and you can't make it to the library, you can send us a payment via PayPal (use this email address) to reimburse us for the cost of replacement. Our library has a lot of funky books that can be hard to access otherwise and we are supported and run by our members.

Thank you for your support,
Your Friendly Neighborhood Librarians`}}};

			for (var type in tempVars) {
				if (tempVars[type].count > 0) {
					var plurality = (tempVars[type].count == 1) ? 'single' : 'plural';
					var subj = encodeURIComponent(template[type][plurality].subj);
					var body = encodeURIComponent(template[type][plurality].body + "\n\n" + template.signature);
					var url = `https://mail.google.com/?view=cm&fs=1&tf=1&source=mailto&su=${subj}&to=${user.user_email}&body=${body}`;
					window.open(url, subj);
				}
			}
		}
    });
}
</script>
<?php get_footer();
//
// EOF: out-list.php
