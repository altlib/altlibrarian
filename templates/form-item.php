<?php
$genres =  get_terms("item_genre", array("parent"=>0));
$user = wp_get_current_user();
$item = array(
    "ID" => "",
    "post_parent" => 0,
    "title" => "",
    "author" => "",
    "publisher" => "",
    "pubdate" => "",
    "genre" => "",
    "subgenre" => "",
    "image" => "",
    "description" => "",
    "status" => "in",
    "popularity" => "0",
    "outdate" => "",
    "last" => "",
);
$cat_id = 2;
$current_sub_id = "";

if (isset($_GET["ID"])) {
    $post = get_post($_GET['ID']);
    if($post){
        $item["ID"] = $_GET['ID'];
        $item["title"] = $post->post_title;
        $item["description"] = $post->post_content;
        $item["post_parent"] = $post->post_parent;
        $meta_values = get_post_meta ($item["ID"]);
        foreach($item as $key=>$value){
            if(isset($meta_values["cf_" . $key][0]))
                $item[$key] = $meta_values["cf_" . $key][0];
        }
        $terms = wp_get_post_terms($item['ID'],  'item_genre');
        foreach($terms as $term){
        	if($term->parent == 0){
        	    $item['genre'] = $term->name;
        	    $cat_id = $term->term_id;
            }else{
				$current_sub_id = $term->term_id;
                $item['subgenre'] = $term->name;
            }
        }
    }
}

$subs = array();

foreach ($genres as $genre) {
	$sub_list = array();
	$subgenres = get_term_children($genre->term_id, "item_genre");
	foreach ($subgenres as $subgenre) {
		$term = get_term_by("id", $subgenre, "item_genre");
		$sub = array(
			"id" => $subgenre
			,"name" => $term->name
		);
		array_push($sub_list, $sub);
	}
	array_push($subs, $sub_list);
}

get_header();
?>
		<!-- <main> -->
				<div class="container mt-3">
					<div class="row">
						<div class="col-12 col-lg-9">
							<form method="post" id="library-form" action="/wp-admin/admin-post.php">
								<input type="hidden" name="action" value="edit_item">
								<input type="hidden" name="Library[ID]" value="<?php echo $item["ID"]; ?>" id="post_id">
								<input type="hidden" name="Library[outdate]" value="<?php echo $item["outdate"]; ?>">
								<input type="hidden" name="Library[post_parent]" value="<?php echo $item["post_parent"]; ?>" id="post_parent">

								<div class="col-12 col-lg-10 offset-lg-2">
<?php
									if ($item["status"] == "unconfirmed") {
?>
										<h3>Accept Item Request</h3>
<?php
									} else {
?>
										<h3><?php echo $item["ID"] ? "Edit" : "Add"; ?> Item
<?php
											if ($item["ID"] && $item["status"] != "req") {
												if($item["post_parent"] == 0) {
?>
													<button class="btn btn-primary btn-sm" type="button" onclick="altlib.library.duplicate();">Duplicate</button>
<?php
												} else {
?>
													<small>Copy of <a href="/add-item/?ID=<?php echo $item["post_parent"];?>"><?php echo $item["post_parent"];?></a></small>
<?php
												}
											}
?>
										</h3>
<?php
									}
									$copy_ids = false;
									if ($item["ID"])
										$copy_ids = $AltLibrarian->get_book_copies($item["ID"]);
									if ($copy_ids) {
?>
										<div class="input-group mb-3">
<?php
											foreach ($copy_ids as $index => $copy_id) {
?>
												<div id="copy_<?php echo $copy_id; ?>">Copy #<?php echo ($index + 2); ?> <button class="btn btn-sm btn-danger" onclick="altlib.library.delete_book(<?php echo $copy_id; ?>); return false;">Delete</button></div>
<?php
											}
?>
										</div>
<?php
									}
?>
								</div>
								<div class="form-group col-12 row required">
									<label class="col-12 col-lg-2 col-form-label text-lg-right" for="library-title">Title</label>
									<div class="col-12 col-lg-10">
										<input type="text" id="library-title" class="form-control form-control-sm" name="Library[title]" value="<?php echo $item['title']; ?>">
									</div>
								</div>
								<div class="form-group col-12 row required">
									<label class="col-12 col-lg-2 col-form-label text-lg-right" for="library-author">Author</label>
									<div class="col-12 col-lg-10">
										<input type="text" id="library-author" class="form-control form-control-sm" name="Library[author]" value="<?php echo $item['author']; ?>">
									</div>
								</div>
								<div class="form-group col-12 row required">
									<label class="col-12 col-lg-2 col-form-label text-lg-right" for="library-publisher">Publisher</label>
									<div class="col-12 col-lg-10">
										<input type="text" id="library-publisher" class="form-control form-control-sm" name="Library[publisher]" value="<?php echo $item['publisher']; ?>">
									</div>
								</div>
								<div class="form-group col-12 row">
									<label class="col-12 col-lg-2 col-form-label text-lg-right" for="library-pubdate">Pubdate</label>
									<div class="col-12 col-lg-10">
										<input type="text" id="library-pubdate" class="form-control form-control-sm" name="Library[pubdate]" value="<?php echo $item['pubdate']; ?>">
									</div>
								</div>
								<div class="form-group col-12 row required">
									<label class="col-12 col-lg-2 col-form-label text-lg-right" for="library-genre">Genre</label>
									<div class="col-12 col-lg-10">
										<select id="library-genre" class="form-control form-control-sm" name="Library[genre]" onchange="altlib.library.update_subgenre('');">
											<option value="">Select a genre</option>
<?php
												foreach ($genres as $g) {
?>
													<option value="<?php echo $g->term_id; ?>" <?php echo ($g->name == $item["genre"] ? "selected" : ""); ?>><?php echo $g->name; ?></option>
<?php
												}
?>
										</select>
									</div>
								</div>
								<div id="library-subgenre-group" class="form-group col-12 row d-none">
									<label class="col-12 col-lg-2 col-form-label text-lg-right" for="library-subgenre">Subgenre</label>
									<div class="col-12 col-lg-10">
										<select id="library-subgenre" class="form-control form-control-sm" name="Library[subgenre]">
											<option id="subgenre-default" value="">Select a subgenre</option>
										</select>
									</div>
								</div>
								<div class="form-group col-12 row required">
									<label class="col-12 col-lg-2 col-form-label text-lg-right" for="library-image">Image URL</label>
									<div class="col-12 col-lg-10">
										<input type="text" id="library-image" class="form-control form-control-sm" name="Library[image]" value="<?php echo $item['image']; ?>">
									</div>
								</div>
								<div class="form-group col-12 row required">
									<label class="col-12 col-lg-2 col-form-label text-lg-right" for="library-description">Description</label>
									<div class="col-12 col-lg-10">
										<textarea id="library-description" class="form-control form-control-sm" name="Library[description]"><?php echo $item['description']; ?></textarea>
									</div>
								</div>
<?php
								if ($item["status"] == "unconfirmed") {
?>
									<input type="hidden" name="Library[status]" value="req">
									<input type="hidden" name="Library[popularity]" value="1">
<?php
								} else {
?>
									<div class="form-group col-12 row required">
										<label class="col-12 col-lg-2 col-form-label text-lg-right" for="library-status">Status</label>
										<div class="col-12 col-lg-10">
										<?php  if (in_array( 'administrator', (array) $user->roles ) ): ?>
											<input type="text" id="library-status" class="form-control form-control-sm" name="Library[status]"  value="<?php echo $item['status']; ?>">
										<?php else: ?>
											<br>
											<input type="hidden" id="library-status" name="Library[status]"  value="<?php echo $item['status']; ?>">
											<?php echo $item['status']; ?>
										<?php endif; ?>
										<?php if($item['status'] == "req" || $item['status'] == "lost"):?>
											<br>
											<button class="btn btn-success btn-sm" type="button" onclick="altlib.library.integrate(<?php echo $item['ID']; ?>, '<?php echo addslashes($item['title']); ?>');">Integrate</button>
										<?php elseif($item['status'] == "in"): ?>
											<br>
											<button class="btn btn-warning btn-sm" type="button" onclick="altlib.library.mark_lost();">Mark as Lost</button>
										<?php endif;?>
										</div>
									</div>
									<div class="form-group col-12 row">
										<label class="col-12 col-lg-2 col-form-label text-lg-right" for="library-popularity">Popularity</label>
										<div class="col-12 col-lg-10">
											<input type="text" id="library-popularity" class="form-control form-control-sm" name="Library[popularity]"  value="<?php echo $item['popularity']; ?>">
										</div>
									</div>
									<div class="form-group col-12 row">
										<label class="col-12 col-lg-2 col-form-label text-lg-right" for="library-last">Author Last Name</label>
										<div class="col-12 col-lg-10">
											<input type="text" id="library-last" class="form-control form-control-sm" name="Library[last]" value="<?php echo $item['last']; ?>">
										</div>
									</div>
									<div id="notice" class="col-12 alert alert-danger d-none"></div>
<?php
								}
								
								if ($item["ID"]) {
?>
									<div class="col-sm-10 offset-sm-2">
										<button type="submit" class="btn btn-primary btn-sm">Save</button>
									</div>
<?php
								} else {
?>
									<div class="col-sm-10 offset-sm-2">
										<button class="btn btn-primary btn-sm" type="button" onclick="altlib.library.create_check($('#library-title').val());">Create</button>
									</div>
<?php 
								}
?>
							</form>
						</div>
					</div>
					<form id="integrate-form" class="d-none" method="post" action="/wp-admin/admin-post.php">
						<input type="hidden" id="action" name="action" value="integrate_request">
						<input type="hidden" id="ID" name="ID" value="<?php echo $_GET["ID"]; ?>">
					</form>
					<form id="lost-form" class="d-none" method="post" action="/wp-admin/admin-post.php">
						<input type="hidden" id="action" name="action" value="mark_as_lost">
						<input type="hidden" id="ID" name="ID" value="<?php echo $_GET["ID"]; ?>">
					</form>
				</div>
		<!-- </main> -->
<script type="text/javascript">
if (typeof $ == "undefined")
    $ = jQuery;

altlib.library.subgenres = <?php echo json_encode($subs); ?>;

$(function() {
	altlib.library.update_subgenre("<?php echo $current_sub_id; ?>");
});

</script>
<?php get_footer(); ?>
