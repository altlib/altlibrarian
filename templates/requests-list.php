<?php

$recalc = isset($_GET["recalc"]);;

$wp_query = new WP_Query(array(
	"posts_per_page"	=> "-1"
	,"post_type"		=> "item"
	,"orderby"			=> "meta_value_num"
	,"order"			=> "DESC"
	,"meta_key"			=> "cf_popularity"
	,"meta_query"		=> array(array(
		"key"		=> "cf_status"
		,"value"	=> "req"
		,"compare"	=> "="))));
		
$available_votes = $AltLibrarian->votes_total(get_current_user_id()) - $AltLibrarian->votes_used(get_current_user_id());

get_header();
?>
		<!-- <main> -->
				<div class="container mt-3">
					<div class="row">
						<div class="col-12 col-lg-9">
							<div class="row">
								<div class="col-12">
										<h3>Requests List</h3>
								</div>
							</div>
<?php
								if (is_user_logged_in()) {
?>
									<div class="row">
										<div class="col-12">
											<div class="alert alert-success">
												You have <?php echo $available_votes.($available_votes == 1 ? " vote" : " votes"); ?>
											</div>
										</div>
									</div>
<?php
								}
								$i = 1;
								
								if (have_posts()) {
									while (have_posts()) {
										the_post();
										$AltLibrarian->get_request($i, $available_votes);
										if ($recalc) {
											$votes = $AltLibrarian->votes_raw(get_the_ID());
											$m_votes = get_post_meta(get_the_ID(), "cf_popularity", true);
											if ($votes != 0 && $votes != $m_votes)
													update_post_meta(get_the_ID(), "cf_popularity", $votes);
										}
										$i++;
									}
								}
?>
						</div>
					</div>
				</div>
				<form id="vote-form" class="d-none" method="post" action="/wp-admin/admin-post.php">
					<input type="hidden" id="action" name="action">
					<input type="hidden" id="ID" name="ID">
					<input type="hidden" id="patron_id" name="patron_id" value="<?php echo get_current_user_id(); ?>">
				</form>
		<!-- </main> -->
<?php get_footer();
