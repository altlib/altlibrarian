<?php
$patrons = $AltLibrarian->get_patrons();

get_header(); ?>
		<!-- <main> -->
				<div class="container mt-3">
					<div class="row">
						<div class="col-12 col-md-9">
							<h3>Patron List</h3>
						</div>
						<div class="col-12 col-md-3">
							<div class="input-group input-group-sm mb-3">
								<div class="input-group-prepend">
									<span class="input-group-text">Search:</span>
								</div>
								<input class="form-control" type="text" oninput="altlib.patron_list.filter_list($(this).val());" value="">
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<div class="table-responsive">
								<table id="patron_list" class="table table-sm table-striped">
									<thead class="thead-light">
										<tr>
<?php
										if (count($patrons) > 0) {
											foreach (get_object_vars($patrons[0]) as $name => $value) {
?>
												<th abbr="<?php echo $name; ?>" onclick="altlib.patron_list.sort(this);"><p class="patron-list-sort btn btn-light btn-sm mb-0"><?php echo $name; ?> </p></th>
<?php
											}
										}
?>
										</tr>
									</thead>
									<tbody>
									</tbody>
								</table>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-6">
							<button id="prev-button" class="btn btn-primary btn-sm d-none" onclick="altlib.patron_list.previous_page();">Previous</a>
						</div>
						<div class="col-6">
							<button id="next-button" class="btn btn-primary btn-sm float-right" onclick="altlib.patron_list.next_page();">Next</a>
						</div>
					</div>
				</div>
		<!-- </main> -->
<script>
if (typeof $ == "undefined")
    $ = jQuery;

altlib.patron_list.patrons_json = <?php echo json_encode($patrons); ?>;
$(document).ready(altlib.patron_list.on_load);
</script>
<?php get_footer();
