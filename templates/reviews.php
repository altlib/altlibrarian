<?php get_header(); ?>
		<!-- <main> -->
				<div class="container mt-3">
					<div class="row">
						<div class="col-12 col-lg-9">
<?php
							global $AltLibrarian;

							$total = $AltLibrarian->get_reviews_count();
							$items_per_page = 20;
							$page = get_query_var("paged");
							$place = $items_per_page * ($page - ($page > 1 ? 1 : 0));

							$comments = $AltLibrarian->get_reviews($place, $items_per_page);
							foreach ($comments as $review) {
								$post = get_post($review->comment_post_ID);
								if (!$post)
									continue;
								$user = get_userdata($review->user_id);
?>
								<div class="row">
									<h3 class="col-12">
										<a href="<?php echo get_permalink($post->ID); ?>"><?php echo $post->post_title; ?></a>
									</h3>
								</div>
								<div class="row">
									<div class="col-2">
										<img class="img-fluid" src="<?php echo get_post_meta($review->comment_post_ID, 'cf_image', true ); ?>" alt="">
									</div>
									<div class="col-10 ">
										<div class="row">
											<div class="col-12">
												Review by <b><?php echo $user->display_name; ?></b>
											</div>
										</div>
										<div class="row mb-2">
											<div class="col-12">
												<div class="review_rate small" data-pixrating="<?php echo $review->comment_karma; ?>"></div>
											</div>
										</div>
										<div class="row">
											<div class="col-12">
												<?php echo "$review->comment_content<br><br>"; ?>
											</div>
										</div>
									</div>
								</div>
<?php
							}
							echo paginate_links(array(
								"format"		=> "page/%#%"
								,"prev_text"	=> __("&laquo;")
								,"next_text"	=> __("&raquo;")
								,"total"		=> floor($total / $items_per_page)
								,"current"		=> max(1, get_query_var("paged"))));
?>
						</div>
					</div>
				</div>
		<!-- </main> -->
<?php get_footer();
