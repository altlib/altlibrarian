<?php
/**
 * Template Name: Item
 **/
$genres = get_terms("item_genre", array("parent" => 0));
$status = "";
$count = 0;
while (have_posts()) {
	the_post();
    $item_status = get_post_meta(get_the_ID(), "cf_status", true);
    $out_date =  intval(get_post_meta( get_the_ID(), 'cf_outdate', true));
    $out_date = date_i18n(get_option("date_format"), $out_date);
		if ($item_status != "req" && $item_status != "lost") {
				$count++;
				if ($item_status == "in") {
					$status .= "Copy #".$count." (".get_the_ID()."): in<br />";
				} else if (is_numeric($item_status)) {
					if (count($posts) > 1) {
						$status .= "Copy #".$count." (".get_the_ID()."): checked out";
						if (current_user_can("manage_patrons"))
							$status .= " to <a href='/profile/".$item_status."'>".get_userdata($item_status)->display_name."</a> on $out_date";
						$status .= '<br />';
					} else {
						$status = 'checked out';
						if (current_user_can("manage_patrons"))
							$status .= " to <a href='/profile/".$item_status."'>".get_userdata($item_status)->display_name."</a> on $out_date";
					}
				} else {
					$status = $item_status;
				}
		} else if ($item_status == "req") {
    		$status = "Requested";
		} else {
    		$status = "Lost";
		}
}
wp_reset_postdata();

get_header();
?>
		<!-- <main> -->
				<div class="container mt-3">
					<div class="row">
						<div class="col-12 col-lg-9">
<?php
							if (have_posts()) {
								the_post();
?>
								<div class="row">
									<div class="float-left col-12 col-sm-4">
										<img class="img-fluid" src="<?php echo get_post_meta(get_the_ID(), "cf_image", true); ?>">
									</div>
									<div class="col-12 col-sm-8">
										<h3><?php echo the_title(); ?></h3>
										<?php echo $AltLibrarian->author_link(get_post_meta(get_the_ID(), "cf_author", true)); ?><br />
										<?php the_content(); ?>
											<table class="table table-striped table-sm">
<?php
												if (current_user_can("manage_patrons")) {
?>
													<thead>
														<tr>
															<th>
																<a class="float-left" href="/add-item/?ID=<?php echo get_the_ID(); ?>"><small>Edit</small></a>
															</th>
															<td></td>
														</tr>
													</thead>
<?php
												}
?>
												<tbody>
													<tr>
														<th class="text-right">status</th>
														<td><?php echo $status; ?></td>
													</tr>
													<tr>
														<th class="text-right">genre</th>
														<td>
															<?php $AltLibrarian->get_genre(); ?>
														</td>
													</tr>
													<tr>
														<th class="text-right">publisher</th>
														<td><?php echo get_post_meta(get_the_ID(), "cf_publisher", true); ?></td>
													</tr>
													<tr>
														<th class="text-right">publish date</th>
														<td><?php echo get_post_meta(get_the_ID(), "cf_pubdate", true); ?></td>
													</tr>
													<tr>
<?php
														if($item_status == "req") {
?>
															<th class="text-right">votes</th>
															<td><?php echo $AltLibrarian->votes_raw(get_the_ID()); ?></td>
<?php
														} else {
?>
															<th class="text-right">popularity</th>
															<td>checked out <?php echo get_post_meta(get_the_ID(), "cf_popularity", true); ?> time(s)</td>
<?php
														}
?>
													</tr>
												</tbody>
											</table>
									</div>
								</div>
								<br />
<?php
								// If comments are open or we have at least one comment, load up the comment template
								if (comments_open() || get_comments_number() != "0") {
?>
									<div class="comments-area">
										<?php comments_template(); ?>
									</div>
<?php
								}
							}
?>
						</div>
						<div class="col-lg-3 d-none d-lg-block">
							<div id="tag-cloud" data-spy="affix" class="affix-top" style="width:240px">
<?php
								$args = array(
									"parent"	=> 0
									,"taxonomy"	=> array("item_genre"));
								wp_tag_cloud($args);
?>
							</div>
						</div>
						<?php $AltLibrarian->genre_dropdown($genres, null); ?>
					</div>
				</div>
		<!-- </main> -->
<?php get_footer();
