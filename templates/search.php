<?php
/**
 * Locations taxonomy archive
 */
$genres =  get_terms("item_genre", array("parent" => 0));

$paged = get_query_var("paged", 1);
if ($paged == 0)
	$paged = 1;

get_header();
?>
		<!-- <main> -->
				<div class="container mt-3">
					<div class="row">
						<div class="col-12 col-lg-9">
							<h3>Search Results for <?php echo $_GET["s"]; ?></h3>
<?php
							if (have_posts()) {
								while (have_posts()) {
									the_post();
									$AltLibrarian->get_item();
								}
								echo paginate_links(array(
									"base"		=> str_replace(9999999, "%#%", esc_url(get_pagenum_link(9999999)))
									,"format"	=> "?paged=%#%"
									,"current"	=> max(1, get_query_var("paged"))
									,"total"	=> $wp_query->max_num_pages));
							} else {
?>
								<h2 class="post-title">No Results Found <?php echo apply_filters( 'the_title', $term->name ); ?></h2>
								<div class="content clearfix">
									<div class="entry">
										<p>...</p>
									</div>
								</div>
<?php
							}
?>
						</div>
						<div class="col-lg-3 d-none d-lg-block">
							<div id="tag-cloud" data-spy="affix" class="affix-top">
<?php
								$args = array(
									"parent"	=> 0
									,"taxonomy"	=> array("item_genre"));
								wp_tag_cloud($args);
?>
							</div>
						</div>
					</div>
					<div class="row">
						<?php $AltLibrarian->genre_dropdown($genres, null); ?>
					</div>
				</div>
		<!-- </main> -->
<?php get_footer();
