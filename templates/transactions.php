<?php
global $AltLibrarian;
$viewModel = $AltLibrarian->transactions_get();

get_header();
if ($viewModel->can_manage_circulation) {
?>
		<!-- <main> -->
				<div class="container mt-3">
					<div class="row">
						<div class="col-12 col-lg-5">
							<div id="<?php echo $viewModel->incoming_chart_id; ?>"></div>
						</div>
						<div class="col-12 col-lg-5">
							<div id="<?php echo $viewModel->outgoing_chart_id; ?>"></div>
						</div>
						<div class="col-12 col-lg-2">
							<button class="btn btn-success btn-sm float-right" data-toggle="modal" data-target="#<?php echo $viewModel->modal_id; ?>">Add Transaction</button>
						</div>
					</div>
					<div class="row">
						<div class="col-12">
							<div id="<?php echo $viewModel->table_id; ?>"></div>
						</div>
					</div>
				</div>
		<!-- </main> -->
<script>
altlib.transaction.transaction_methods = <?php echo $viewModel->transaction_methods; ?>;
altlib.transaction.transaction_types = <?php echo $viewModel->transaction_types; ?>;
altlib.transaction.transactions = <?php echo $viewModel->transactions; ?>;
altlib.transaction.patrons = <?php echo $viewModel->patrons; ?>;
altlib.transaction.profile_link = <?php echo $viewModel->profile_link; ?>;
altlib.transaction.is_admin = <?php echo json_encode($viewModel->is_admin); ?>;

altlib.transaction.on_load("<?php echo $viewModel->incoming_chart_id; ?>", "<?php echo $viewModel->outgoing_chart_id; ?>", "<?php echo $viewModel->table_id; ?>", "<?php echo $viewModel->modal_id; ?>");
</script>
<?php
} else {
?>
    You're not suppose to be here..
<?php
}

get_footer();
