<?php
$votes_left = ceil($AltLibrarian->votes_total(get_current_user_id()) - $AltLibrarian->votes_used(get_current_user_id()));

get_header();
?>
	<!-- <main> -->
			<div class="container mt-3">
<?php
				if ($votes_left > 0 && !isset($_GET["thanks"])) {
?>
					<div class="row">
						<div class="col-12 col-lg-9">
							<form method="post" action="/wp-admin/admin-post.php">
								<input type="hidden" name="action" value="edit_item">
								<input type="hidden" name="Library[status]" value="unconfirmed">
								<input type="hidden" name="Library[popularity]" value="1">
								<div class="col-12 col-lg-10 offset-lg-2">
									<h3>Make A Request</h3>
								</div>
								<div class="form-group col-12 row required">
									<label class="col-12 col-lg-2 col-form-label text-lg-right">Title</label>
									<div class="col-12 col-lg-10">
										<input class="form-control form-control-sm" type="text" name="Library[title]" value="">
									</div>
								</div>
								<div class="form-group col-12 row required">
									<label class="col-12 col-lg-2 col-form-label text-lg-right">Author</label>
									<div class="col-12 col-lg-10">
										<input class="form-control form-control-sm" type="text" name="Library[author]" value="">
									</div>
								</div>
								<div class="form-group col-12 row required">
									<label class="col-12 col-lg-2 col-form-label text-lg-right">Description</label>
									<div class="col-12 col-lg-10">
										<textarea class="form-control form-control-sm" name="Library[description]"></textarea>
									</div>
								</div>
								<div class="col-12 col-lg-10 offset-lg-2">
									<button class="btn btn-primary btn-sm" type="submit">Create</button>
								</div>
							</form>
						</div>
					</div>
<?php
				} else {
?>
					<div class="row">
						<div class="alert alert-success">
							<?php if(!isset($_GET['thanks'])): ?>
							You don't have enough votes to make a request.   Remove a vote and try again!
							<?php else: ?>
							Thanks for your request!  Our volunteers will consider it for approval.
							<?php endif; ?>
						</div>
					</div>
<?php
				}
?>
			</div>
	<!-- </main> -->
<?php get_footer();
