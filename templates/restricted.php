<?php get_header(); ?>
		<!-- <main> -->
				<div class="container mt-3">
					<div class="row">
						<div class="col-12 col-lg-9">
<?php
							if (current_user_can("manage_circulation")) {
								while (have_posts()) {
									the_post();
									the_content();
								}
							} else {
?>
								You're not suppose to be here..
<?php
							}
?>
						</div>
					</div>
				</div>
		<!-- </main> -->
<?php get_footer();
