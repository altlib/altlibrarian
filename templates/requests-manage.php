<?php get_header(); ?>
		<!-- <main> -->
				<div class="container mt-3">
<?php
					if (current_user_can("manage_circulation")) {
						$wp_query = new WP_Query(array(
							"posts_per_page"	=> "-1"
							,"post_type"		=> "item"
							,"orderby"			=> "ID"
							,"order"			=> "ASC"
							,"meta_query"		=> array(array(
								"key"		=> "cf_status"
								,"value"	=> "unconfirmed"
								,"compare"	=> "="))));
?>
						<div class="row">
							<div class="col-12 col-lg-9" role="main">
								<h3>Accept Requests</h3>
								<table class="table table-striped table-sm">
<?php
									if (have_posts()) {
										while (have_posts()) {
											the_post();
?>
											<tr>
												<td>
													<a href="<?php the_permalink(); ?>"><?php echo the_title(); ?></a>
												</td>
												<td>
													<div id="accept_decline_buttons" class="btn-group float-right">
														<button class="btn btn-success btn-sm" onclick="altlib.request.manage.accept(<?php echo get_the_ID(); ?>);">Accept</button>
														<button class="btn btn-danger btn-sm" onclick="altlib.request.manage.decline_prompt(<?php echo get_the_ID(); ?>);">Decline</button>
													</div>
													<div id="decline_box_<?php echo get_the_ID(); ?>" class="d-none">
														<form>
															<select class="form-control form-control-sm" onchange="altlib.request.manage.decline_reason(<?php echo get_the_ID(); ?>, this.value, this);">
																<option value="" default>Choose message</option>
																<option value=1>The book is already in the collection (checked in)</option>
																<option value=2>The book is already in the collection (checked out)</option>
																<option value=3>The book is already on the request list</option>
															</select>
															<textarea id="decline_text_<?php echo get_the_ID(); ?>" class="form-control form-control-sm"></textarea>
															<div class="btn-group">
																<button class="btn btn-danger btn-sm" type="button" onclick="altlib.request.manage.decline(<?php echo get_the_ID(); ?>);">Send Email and Remove Item</button>
																<button class="btn btn-secondary btn-sm" type="button" onclick="altlib.request.manage.decline_cancel(<?php echo get_the_ID(); ?>);">Cancel</button>
															</div>
														</form>
													</div>
												</td>
											</tr>
<?php
										}
									}
?>
								</table>
							</div>
						</div>
<?php
						$wp_query = new WP_Query(array(
							"posts_per_page"	=> "-1"
							,"post_type"		=> "item"
							,"orderby"			=> "ID"
							,"order"			=> "ASC"
							,"meta_query"		=> array(array(
								"key"		=> "cf_status"
								,"value"	=> "req"
								,"compare"	=> "="))));
?>
						<div class="row">
							<div class="col-12 col-lg-9" role="main">
								<h3>Manage Requests</h3>
								<table class="table table-striped table-sm">
<?php
									if (have_posts()) {
										while (have_posts()) {
											the_post();
?>
											<tr>
												<td>
													<a href="<?php the_permalink(); ?>"><?php echo the_title(); ?></a>
												</td>
												<td>
													<button class="btn btn-success btn-sm float-right" onclick="altlib.library.integrate(<?php echo get_the_ID(); ?>, '<?php echo get_the_title(); ?>');">Integrate</button>
												</td>
											</tr>
<?php
										}
									}
?>
								</table>
							</div>
						</div>
<?php
					} else {
?>
						<div class="row">You're not supposed to be here...</div>
<?php
					}
?>
				</div>
				<form id="decline-form" class="d-none" method="post" action="/wp-admin/admin-post.php">
					<input id="action"	type="hidden"	name="action"	value="delete_item">
					<input id="ID"		type="hidden"	name="ID">
					<input id="content"	type="hidden"	name="content">
				</form>
				<form id="integrate-form" class="d-none" method="post" action="/wp-admin/admin-post.php">
					<input id="action"	type="hidden"	name="action" value="integrate_request">
					<input id="ID"		type="hidden"	name="ID">
				</form>
		<!-- </main> -->
<?php get_footer();
