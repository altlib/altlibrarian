<?php

global $AltLibrarian;

$items = $AltLibrarian->get_requested_books();
	
$patrons = get_users(array("fields" => array("id", "display_name")));
$votes = array();

foreach ($patrons as $index => $patron) {
    $votes_used = $AltLibrarian->votes_used($patron->id);
    $votes_total = $AltLibrarian->votes_total($patron->id);
    $votes[$patron->id] = $votes_total - $votes_used;
    if ($votes[$patron->id] <= 0)
        unset($patrons[$index]);
}

get_header();
?>
		<!-- <main> -->
				<div class="container mt-3">
					<div class="row">
						<div class="col-12 col-lg-9">
							<form method="post" action="/wp-admin/admin-post.php">
								<input type="hidden" name="action" value="vote_for_them">
								<div class="col-12 col-lg-10 offset-lg-2">
									<h3>Vote For Them</h3>
								</div>
								<div class="form-group col-12 row required">
									<label class="col-12 col-lg-2 col-form-label text-lg-right">Requested Item</label>
									<div class="col-12 col-lg-10">
										<select class="form-control form-control-sm" name="ID">
											<option value="">Select an item</option>
<?php
												foreach ($items as $item) {
?>
													<option value="<?php echo $item->ID; ?>"><?php echo $item->post_title; ?></option>
<?php
												}
?>
										</select>
									</div>
								</div>
								<div class="form-group col-12 row required">
									<label class="col-12 col-lg-2 col-form-label text-lg-right">Patron</label>
									<div class="col-12 col-lg-10">
										<select class="form-control form-control-sm" name="user_id">
											<option value="">Select a patron</option>
<?php
												foreach ($patrons as $patron) {
?>
													<option value="<?php echo $patron->id; ?>"><?php echo $patron->display_name." (".$votes[$patron->id].")"; ?></option>
<?php
												}
?>
										</select>
									</div>
								</div>
								<div class="col-12 col-lg-10 offset-lg-2">
									<button class="btn btn-primary btn-sm" type="submit">Vote</button>
								</div>
							</form>
						</div>
					</div>
				</div>
		<!-- </main> -->
<?php get_footer();
