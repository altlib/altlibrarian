<?php
/**
 * Locations taxonomy archive
 */
$term = get_term_by("slug", get_query_var("term"), get_query_var("taxonomy"));
$genres = get_terms("item_genre", array("parent" => 0));

if ($term !== false) {
	if ($term->parent == 0) {
		$genre = $term;
		$children = get_term_children($term->term_id, get_query_var("taxonomy"));
	} else {
		$parent = get_term_by("ID", $term->parent, "item_genre");
		$genre = $parent;
		$children = get_term_children($term->parent, get_query_var("taxonomy"));
	}
} else {
	$parent = array();
	$genre = $parent;
	$children = array();
}

get_header();
?>
		<!-- <main> -->
				<div class="container mt-3">
					<div class="row">
						<?php $AltLibrarian->genre_dropdown($genres, $genre); ?>
					</div>
					<div class="row">
						<div class="col-12 col-lg-9">
<?php
							if ($genre) {
?>
								<h3 class="d-none d-lg-block"><?php echo esc_html($genre->name); ?></h3>
								<ol class="breadcrumb">
<?php
									foreach ($children as $termId) {
										$childterm = get_term_by("id", $termId, get_query_var("taxonomy"));
										if ($childterm->name != $term->name) {
?>
											<li class="breadcrumb-item"><a href="/browse/<?php echo $term->slug; ?>/<?php echo $childterm->slug; ?>"><?php echo $childterm->name; ?></a></li>
<?php
										} else {
?>
											<li class="breadcrumb-item active"><?php echo $childterm->name; ?></li>
<?php
										}
									}
?>
								</ol>
<?php
							} else {
?>
								<h3>New Arrivals</h3>
<?php
							}
							
							if (!$term) {
								$args = array(
									"showposts"				=> 20
									,"ignore_sticky_posts"	=> 1
									,"post_type"			=> "item"
									,"post_status"			=> "publish"
									,"paged"				=> get_query_var("paged")
									,"orderby"				=> "post_modified"
									,"order"				=> "DESC"
									,"meta_query"			=> array(array(
										"key"		=> "cf_status"
										,"value"	=> "unconfirmed"
										,"compare"	=> "!=")));
								$wp_query = new WP_Query ($args);
							}
							
							if (have_posts()) {
								while (have_posts()) {
									the_post();
									$AltLibrarian->get_item();
								}
								echo paginate_links(array(
									"base"		=> str_replace(9999999, "%#%", esc_url(get_pagenum_link(9999999)))
									,"format"	=> "?paged=%#%"
									,"current"	=> max(1, get_query_var("paged"))
									,"total"	=> $wp_query->max_num_pages));
							} else {
?>
								<h2 class="post-title">No News in <?php echo apply_filters("the_title", $term->name); ?></h2>
								<div class="content clearfix">
									<div class="entry">
										<p>It seems there isn't anything happening in <strong><?php echo apply_filters("the_title", $term->name); ?></strong> right now. Check back later, something is bound to happen soon.</p>
									</div>
								</div>
<?php
							}
?>
						</div>
						<div class="col-lg-3 d-none d-lg-block">
							<div id="tag-cloud" data-spy="affix" class="affix-top">
<?php
								$args = array(
									"parent"	=> 0
									,"taxonomy"	=> array("item_genre"));
								wp_tag_cloud($args);
?>
							</div>
						</div>
					</div>
				</div>
<?php get_footer();
