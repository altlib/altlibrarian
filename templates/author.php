<?php

$curauth = get_userdata(intval($author));
$meta = get_user_meta($author);
$dues = $AltLibrarian->get_membership_fee($curauth->ID);
$last_pay = $AltLibrarian->get_last_pay($curauth->ID);
$due = $last_pay["due"];
$danger = $last_pay["danger"] ? "dues-danger" : "dues-success";
$isLifetime = $last_pay["isLifetime"];
$inactive = $last_pay["inactive"];

get_header();
?>
		<!-- <main> -->
				<div class="container mt-3">
					<div class="row">
						<div class="col-12 col-lg-5">
<?php
							if (get_current_user_id() == $curauth->ID) {
?>
								<a class="float-right" href="/edit-profile/"><small>Edit Profile</small></a>
<?php
							} else {
?>
								<small>&nbsp;</small>
<?php
							}
?>
							<table class="table table-striped table-sm">
								<tr>
									<td style="width:100px; height:124px;"><?php echo get_avatar($author); ?></td>
									<td>
										<h3><?php if (in_array("nickname", $meta)) echo $meta["nickname"][0]; ?></h3>
<?php
										if ($dues >= 100) {
?>
											<p>Lifetime Member</p>
<?php
										}
?>
										<p><?php if (in_array("description", $meta)) echo $meta["description"][0]; ?></p>
									</td>
								</tr>
<?php
								if (current_user_can("manage_circulation") || get_current_user_id() == $curauth->ID) {
?>
									<tr>
										<th class="text-right">Votes</th>
										<td><?php echo $AltLibrarian->votes_used($curauth->ID); ?> / <?php echo $AltLibrarian->votes_total($curauth->ID); ?></td>
									</tr>
									<tr class="<?php echo $danger; ?>">
										<th class="text-right">Membership</th>
										<td>
<?php
											if (current_user_can("manage_circulation")) {
?>
												<form id="transaction-form" class="form-inline justify-content-between" action="/wp-admin/admin-post.php" method="post">
													<input type="hidden" name="action" value="pay_patron">
													<input id="transaction-patron_id" type="hidden" name="ID" value="<?php echo $curauth->ID; ?>">
													$<?php echo $dues; ?>
<?php
													if ($last_pay && $due) {
?>
														($5 due <?php echo ($due < time() ? human_time_diff($due)." ago" : "in ".human_time_diff($due)); ?>)
<?php
													} elseif (!$last_pay) {
?>
														(Haven't paid in a while...)
<?php
													}
?>
													<div class="input-group float-right">
														<input id="transaction-amount" class="form-control form-control-sm" name="amount" type="text" pattern="[0-9]+(\.[0-9]{0,2})?" title="This must be a number with up to 2 decimal places" style="width:40px;">
														<div class="input-group-append">
															<button type="submit" class="btn btn-success btn-sm">Pay</button>
														</div>
													</div>
												</form>
<?php
											}
?>
										</td>
									</tr>
<?php
								}
?>
							</table>
						</div>
						<div class="col-12 col-lg-7">
<?php
							if (current_user_can("manage_circulation")) {
?>
								<a class="float-right" href="/add-patron/?ID=<?php echo $curauth->ID; ?>"><small>Edit Patron Info</small> </a>
<?php
							} else {
?>
								<small>&nbsp;</small>
<?php
							}
							
							if (current_user_can("manage_circulation") || get_current_user_id() == $curauth->ID) {
?>
							<table class="table table-striped table-sm">
								<tr>
									<th class="text-right">Full Name</th>
									<td><?php echo $curauth->first_name." ".$curauth->last_name; ?></td>
								</tr>
								<tr>
									<th class="text-right">Email</th>
									<td><?php echo $curauth->user_email; ?></td>
								</tr>
								<tr>
									<th class="text-right">Phone</th>
									<td><?php if (in_array("phone", $meta)) echo $meta["phone"][0]; ?></td>
								</tr>
								<tr>
									<th class="text-right">Join Date</th>
									<td><?php echo $curauth->user_registered; ?></td>
								</tr>
								<tr>
									<th class="text-right">Last Activity</th>
									<td><?php echo date("Y-m-d H:i:s", $meta["last_activity"][0]); ?></td>
								</tr>
								<tr>
									<th class="text-right">Notes</th>
									<td><?php if (in_array("notes", $meta)) echo $meta["notes"][0]; ?></td>
								</tr>
							</table>
<?php
							}
?>
						</div>
					</div>
<?php
					if (current_user_can("manage_circulation") || get_current_user_id() == $curauth->ID) {
?>
						<div class="row">
							<div class="col-12 mb-3">
								<div class="card">
									<div id="out-books-header" class="card-header text-white">
										<h5>Out books</h5>
									</div>
									<div class="card-body">
										<div id="checked" >
											<?php $AltLibrarian->ajax_checked_items($curauth->ID); ?>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-12 mb-3">
								<div class="card">
									<div id="votes-header" class="card-header text-white">
										<h5>Votes</h5>
									</div>
									<div class="card-body">
										<div id="checked" >
											<?php $AltLibrarian->ajax_voted_items($curauth->ID); ?>
										</div>
									</div>
								</div>
							</div>
						</div>
<?php
					}
					
					if (current_user_can("manage_circulation")) {
?>
						<div class="row">
							<div class="col-12 mb-3">
								<div class="card">
									<div id="check-out-header" class="card-header text-white">
										<h5>Check out books</h5>
									</div>
									<div class="card-body">
										<div id="preview"></div>
<?php
										if ($inactive) {
?>
											This account has been marked as inactive.
<?php
										} else {
?>
											<input id="checkout-field" class="form-control form-control-sm typeahead" type="text" placeholder="Type In Book Title">
<?php
										}
?>
									</div>
								</div>
							</div>
						</div>
<?php
					}
?>
				</div>
				<!-- Modal -->
				<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
					<div class="modal-dialog" role="document">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title" id="myModalLabel">Add Note</h4>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
									<span aria-hidden="true">&times;</span>
								</button>
							</div>
							<div class="modal-body">
								<form id="note-form" class="form-horizontal">
									<input type="hidden" name="action" value="save_note">
									<input type="hidden" id="outnotes-item_id" name="OutNotes[item_id]" value="">
									<input type="hidden" id="outnotes-patron_id" name="OutNotes[patron_id]" value="">
									<div id="out_title"></div>
									<div class="row">
										<div class="col-12 form-group field-outnotes-comments">
											<label class="col-12 control-label" for="outnotes-comments">Comments</label>
											<div class="col-12">
												<textarea id="outnotes-comments" class="form-control form-control-sm" name="OutNotes[comments]" rows="6" cols="15"></textarea>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-12 form-group field-outnotes-color required">
											<label class="col-12 control-label" for="outnotes-color">Color</label>
											<div class="col-12">
												<div id="outnotes-color">
													<label class="radio-inline"><input type="radio" name="OutNotes[color]" value="green"><font color="green">green</font></label> <label class="radio-inline"><input type="radio" name="OutNotes[color]" value="red" checked><font color="red">red</font></label> <label class="radio-inline"><input type="radio" name="OutNotes[color]" value="blue"><font color="blue">blue</font></label>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary btn-sm" data-dismiss="modal">Close</button>
								<button type="button" class="btn btn-primary btn-sm" onclick="altlib.note.save();">Create Note</button>
							</div>
						</div>
					</div>
				</div>
		<!-- </main> -->
<script type="text/javascript">
if(typeof $ == "undefined")
    $ = jQuery;

altlib.library.patron_id = <?php echo $curauth->ID; ?>;

url = "http://" + document.location.host;
<?php
	if (current_user_can('manage_circulation')) {
?>
		$(function(){
			var libraryItem = new Bloodhound({
				datumTokenizer:		Bloodhound.tokenizers.obj.whitespace("value")
				,queryTokenizer:	Bloodhound.tokenizers.whitespace
				,remote: {
					url:		"/wp-admin/admin-ajax.php"
					,replace:	function(url, query) {
						return url + "?action=list_items&term=" + query;
					},ajax: { type: "POST" }}
				,limit: 10});

			libraryItem.initialize();

			$('.typeahead').typeahead({
				minLength: 1,
				highlight: true
				},{
				name: 'check-out-item',
				displayKey: 'value',
				source: libraryItem.ttAdapter()
			}).bind('typeahead:selected', function(obj, datum, name) {
				$.post(
					"/wp-admin/admin-ajax.php"
					,{
						action:	"display_item"
						,id:	datum.id }
					,function(result) {
						$("#preview").html(result);
					});});
		});
<?php
	}
?>
</script>
<?php get_footer(); ?>
