<?php
$meta_values = array(
    "first_name" => "",
    "last_name" => "",
    "membership_fee" => "",
    "phone" => "",
    "notes" => "",''
);
$dues = 0;

if (isset($_GET['ID'])) {
    $user_info = get_userdata($_GET['ID']);
    if ($user_info) {
        foreach ($meta_values as $key=>&$value) {
            $value = get_user_meta($user_info->ID, $key, true);
        }
    }
    $dues = $AltLibrarian->get_membership_fee($user_info->ID);
}

get_header();
?>
		<!-- <main> -->
				<div class="container mt-3">
					<div class="row">
						<div class="col-12 col-lg-9">
							<form method="post" action="/wp-admin/admin-post.php">
								<input type="hidden" name="ID" value="<?php echo (isset($_GET['ID']) ? $_GET['ID'] : ""); ?>">
								<input type="hidden" name="action" value="edit_user">
								<div class="col-12 col-lg-10 offset-lg-2">
									<h3><?php echo (isset($_GET["ID"]) ? "Edit" : "Add"); ?> Patron</h3>
								</div>
								<div class="form-group col-12 row">
									<label class="col-12 col-lg-2 col-form-label text-lg-right" for="patron-name">First Name</label>
									<div class="col-12 col-lg-10">
										<input type="text" id="patron-name" class="form-control form-control-sm" name="first_name" value="<?php echo $meta_values['first_name']; ?>">
									</div>
								</div>
								<div class="form-group col-12 row">
									<label class="col-12 col-lg-2 col-form-label text-lg-right" for="patron-name">Last Name</label>
									<div class="col-12 col-lg-10">
										<input type="text" id="patron-name" class="form-control form-control-sm" name="last_name" value="<?php echo $meta_values['last_name']; ?>">
									</div>
								</div>
								<div class="form-group col-12 row">
									<label class="col-12 col-lg-2 col-form-label text-lg-right" for="patron-email">Email</label>
									<div class="col-12 col-lg-10">
										<input class="form-control form-control-sm" name="user_email" type="text" id="user_email" value="<?php echo (isset($user_info->user_email) ? $user_info->user_email : ""); ?>" aria-required="true">
									</div>
								</div>
								<div class="form-group col-12 row">
									<label class="col-12 col-lg-2 col-form-label text-lg-right" for="patron-phone">Phone</label>
									<div class="col-12 col-lg-10">
										<input type="text" id="patron-phone" class="form-control form-control-sm" name="phone" value="<?php echo $meta_values['phone']; ?>">
									</div>
								</div>
								<div class="form-group col-12 row">
									<label class="col-12 col-lg-2 col-form-label text-lg-right" for="patron-notes">Notes</label>
									<div class="col-12 col-lg-10">
										<textarea class="form-control form-control-sm" name="notes"><?php echo $meta_values['notes']; ?></textarea>
									</div>
								</div>
								<div class="form-group col-12 row">
									<label class="col-12 col-lg-2 col-form-label text-lg-right" for="patron-fee">Fee</label>
									<div class="col-12 col-lg-10">
										<input type="text" id="patron-fee" class="form-control form-control-sm" name="membership_fee" value="<?php echo $dues; ?>" <?php if ( !current_user_can( 'administrator' )): ?>readonly<?php endif; ?>>
									</div>
								</div>
								<div class="col-sm-10 offset-sm-2">
									<button class="btn btn-primary btn-sm" type="submit"><?php echo (isset($_GET['ID']) ? "Save" : "Create"); ?></button>
								</div>
							</form>
						</div>
					</div>
				</div>
		<!-- </main> -->
<?php get_footer();
