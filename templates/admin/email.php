<?php
global $AltLibrarian;
?>
<div class="wrap">
    <h1>Email</h1>
    <form method="post" action="<?php echo esc_html(admin_url('admin-post.php')); ?>">
        <input type="hidden" name="action" value="admin_update_email"></input>
        <div class="form-group">
            <label for="addressInput">Email address</label>
            <input id="addressInput" name="address" class="form-control" type="email" value="<?php echo $AltLibrarian->admin_get_email(); ?>"></input>
        </div>
        <div class="form-group">
            <label for="signatureInput">Signature</label>
            <textarea id="signatureInput" name="signature" class="form-control" type="textbox" rows="10"><?php echo $AltLibrarian->admin_get_emailSig(); ?></textarea>
        </div>
        <button type="submit" class="btn btn-sm btn-primary">Submit</button>
    </form>
</div>
