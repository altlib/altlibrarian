<?php
global $AltLibrarian;
?>
<div class="wrap">
    <h1>Branding</h1>
    <form method="post" action="<?php echo esc_html(admin_url('admin-post.php')); ?>">
        <input type="hidden" name="action" value="admin_update_branding"></input>
        <div class="form-group">
            <label for="shortInput">Short Name</label>
            <input id="shortInput" name="shortName" class="form-control" type="text" value="<?php echo $AltLibrarian->admin_get_shortName(); ?>"></input>
        </div>
        <div class="form-group">
            <label for="longInput">Long Name</label>
            <input id="longInput" name="longName" class="form-control" type="text" value="<?php echo $AltLibrarian->admin_get_longName(); ?>"></input>
        </div>
        <button type="submit" class="btn btn-sm btn-primary">Submit</button>
    </form>
</div>
