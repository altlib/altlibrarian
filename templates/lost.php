<?php get_header(); ?>
	<!-- <main> -->
			<div class="container mt-3">
				<div class="row">
<?php
					if (current_user_can("manage_circulation")) {
						$genres =  get_terms("item_genre", array("parent" => 0));
?>
						<div id="content" class="col-12 col-lg-9" role="main">
							<h3>Lost Books</h3>
							<table class="table table-striped table-sm">
<?php
								foreach ($genres as $genre) {
									$wp_query = new WP_Query(array(
										"posts_per_page"	=> "-1"
										,"post_type"		=> "item"
										,"orderby"			=> "ID"
										,"order"			=> "ASC"
										,"meta_query"		=> array(array(
											"key"		=> "cf_status"
											,"value"	=> "lost"
											,"compare"	=> "="))
										,"tax_query"		=> array(array(
											"taxonomy"	=> "item_genre"
											,"field"	=> "term_id"
											,"terms"	=> array($genre->term_id)
											,"operator"	=> "IN"))));
									if (have_posts()) {
?>
										<tr>
											<td colspan=2><h3><?php echo $genre->name; ?></h3></td>
										</tr>
<?php
										while (have_posts()) {
											the_post();
?>
											<tr>
												<td><a href="<?php the_permalink(); ?>">
<?php
													echo the_title()." - ";
													$lost_ID = get_the_ID();
													$lost_authors = get_post_meta($lost_ID, "cf_author");
													foreach ($lost_authors as $index => $lost_author) {
														echo preg_replace("/\s\([^)]+\)/", "", $lost_author);
														if ($index < count($lost_authors) - 1)
															echo ", ";
													}
?>
												</a></td>
												<td>
													<button class="btn btn-success btn-sm" onclick="altlib.library.integrate(<?php echo $lost_ID; ?>, '<?php echo get_the_title(); ?>');">Integrate</button>
												</td>
											</tr>
<?php
										}
									}
								}
?>
							</table>
						</div>
<?php
					} else {
?>
						You're not supposed to be here...
<?php
					}
?>
				</div>
			</div>
			<form id="integrate-form" class="d-none" method="post" action="/wp-admin/admin-post.php">
				<input id="action" name="action" type="hidden" value="integrate_request">
				<input id="ID" name="ID" type="hidden">
			</form>
	<!-- </main> -->
<?php get_footer();
